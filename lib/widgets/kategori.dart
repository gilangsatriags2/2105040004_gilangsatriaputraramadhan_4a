import 'package:flutter/material.dart';
import 'package:login_app_hci/views/theme.dart';

class Kategori extends StatefulWidget {
  final String? logo;
  final String? kategori;
  final Color backgroundColor;
  final Color borderColor;
  final Color? textColor;
  final VoidCallback onpressed;
  final double? height;
  const Kategori({
    Key? key,
    required this.logo,
    required this.kategori,
    required this.backgroundColor,
    required this.borderColor,
    required this.textColor,
    required this.onpressed,
    required this.height,
  }) : super(key: key);

  @override
  State<Kategori> createState() => _KategoriState();
}

class _KategoriState extends State<Kategori> {
  @override
  Widget build(BuildContext context) {
    return IntrinsicWidth(
      child: Container(
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(10),
          border: Border.all(
            color: widget.borderColor,
          ),
          color: widget.backgroundColor,
        ),
        height: widget.height,
        width: widget.logo == "more" || widget.logo == "" ? 40 : null,
        child: TextButton(
          onPressed: widget.onpressed,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              if (widget.logo != null)
                Image.asset("images/icons/${widget.logo}.png"),
              if (widget.kategori != null)
                SizedBox(
                  width: 5,
                ),
              if (widget.kategori != null || widget.kategori == "")
                Text(
                  "${widget.kategori}",
                  style: TextStyle(
                    fontFamily: "PoppinsRegular",
                    fontSize: 11,
                    color: widget.textColor == null
                        ? blackColor
                        : widget.textColor,
                  ),
                ),
            ],
          ),
        ),
      ),
    );
  }
}
