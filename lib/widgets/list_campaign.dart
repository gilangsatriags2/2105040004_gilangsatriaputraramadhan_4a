import 'package:expandable_text/expandable_text.dart';
import 'package:flutter/material.dart';
import 'package:login_app_hci/views/theme.dart';
import 'package:step_progress_indicator/step_progress_indicator.dart';

class ListCampaign extends StatefulWidget {
  final String image;

  final double heightPicture;
  final double widthPicture;
  final String campaigner;

  final String campaign;
  const ListCampaign(
      {super.key,
      required this.image,
      required this.heightPicture,
      required this.widthPicture,
      required this.campaigner,
      required this.campaign});

  @override
  State<ListCampaign> createState() => _ListCampaignState();
}

class _ListCampaignState extends State<ListCampaign> {
  @override
  Widget build(BuildContext context) {
    return Container(
      width: 396,
      child: Column(
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Container(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Image.asset(
                      "images/${widget.image}.png",
                      width: widget.widthPicture,
                      height: widget.heightPicture,
                    ),
                  ],
                ),
              ),
              Container(
                width: 238,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    Padding(
                      padding: const EdgeInsets.only(
                          right: 10.0, top: 3.0, bottom: 3.0, left: 6),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          ExpandableText(
                            "${widget.campaign}",
                            expandText: 'more',
                            linkColor: greenColor,
                            collapseText: 'less',
                            maxLines: 2,
                            style: TextStyle(
                              fontFamily: "PoppinsSB",
                              fontSize: 10.0,
                              color: blackColor,
                            ),
                          ),
                          SizedBox(
                            height: 3,
                          ),
                          Row(
                            children: [
                              Text(
                                "${widget.campaigner}",
                                style: TextStyle(
                                  fontFamily: "PoppinsRegular",
                                  fontSize: 10.0,
                                  color: blackColor,
                                ),
                              ),
                              SizedBox(
                                width: 7.0,
                              ),
                              Image.asset("images/icons/check.png"),
                            ],
                          ),
                          SizedBox(
                            height: 3,
                          ),
                          SizedBox(
                            width: 190,
                            child: StepProgressIndicator(
                              totalSteps: 100,
                              currentStep: 50,
                              size: 5,
                              padding: 0,
                              selectedColor: Colors.yellow,
                              unselectedColor: Colors.cyan,
                              roundedEdges: Radius.circular(10),
                              selectedGradientColor: LinearGradient(
                                begin: Alignment.topLeft,
                                end: Alignment.bottomRight,
                                colors: [
                                  Color.fromRGBO(86, 191, 191, 100),
                                  Color.fromRGBO(86, 191, 191, 100)
                                ],
                              ),
                              unselectedGradientColor: LinearGradient(
                                begin: Alignment.topLeft,
                                end: Alignment.bottomRight,
                                colors: [
                                  Color.fromRGBO(204, 204, 204, 100),
                                  Color.fromRGBO(204, 204, 204, 100),
                                ],
                              ),
                            ),
                          ),
                          SizedBox(
                            height: 3,
                          ),
                          RichText(
                            text: TextSpan(
                              text: "Terkumpul ",
                              style: TextStyle(
                                fontFamily: "PoppinsRegular",
                                fontSize: 10.0,
                                color: blackColor,
                              ),
                              children: [
                                TextSpan(
                                  text: "Rp. 10.500.000,00",
                                  style: TextStyle(
                                    fontFamily: "PoppinsSB",
                                    fontSize: 10,
                                    color: blackColor,
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
          Divider(
            thickness: 1,
            color: greenColor,
          ),
        ],
      ),
    );
  }
}
