import 'package:expandable_text/expandable_text.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:login_app_hci/views/detail_campaign.dart';
import 'package:login_app_hci/views/theme.dart';
import 'package:step_progress_indicator/step_progress_indicator.dart';

class CardDonasi extends StatefulWidget {
  const CardDonasi({super.key});

  @override
  State<CardDonasi> createState() => _CardDonasiState();
}

class _CardDonasiState extends State<CardDonasi> {
  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {
        Navigator.push(
          context,
          MaterialPageRoute(
            builder: (context) => DetailCampaign(),
          ),
        );
      },
      child: Container(
        decoration: BoxDecoration(
          color: whiteColor,
          borderRadius: BorderRadius.circular(10),
          boxShadow: [
            BoxShadow(
              color: greyColor,
              offset: Offset(4, 4),
              blurRadius: 5,
            ),
          ],
        ),
        width: 219.0,
        child: Padding(
          padding: const EdgeInsets.only(top: 15, right: 15.0, left: 15.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              ClipRRect(
                  borderRadius: BorderRadius.circular(10),
                  child: Image.asset("images/paul.png")),
              SizedBox(
                height: 6.0,
              ),
              ExpandableText(
                "Bantu kakek paul untuk mendapatkan tempat tinggal",
                expandText: 'more',
                linkColor: greenColor,
                collapseText: 'less',
                maxLines: 2,
                style: TextStyle(
                  fontFamily: "PoppinsMedium",
                  fontSize: 9.0,
                  color: blackColor,
                ),
              ),
              SizedBox(
                height: 6.0,
              ),
              Text(
                "Terkumpul Rp. 5.000.000,00",
                style: TextStyle(
                  fontFamily: "PoppinsMedium",
                  fontSize: 9.0,
                  color: greenColor,
                ),
              ),
              Text(
                "Target Rp. 10.000.000,00",
                style: TextStyle(
                  fontFamily: "PoppinsMedium",
                  fontSize: 9.0,
                  color: Color(0x0ff8e8e8e),
                ),
              ),
              SizedBox(
                height: 7.0,
              ),
              StepProgressIndicator(
                totalSteps: 100,
                currentStep: 50,
                size: 5,
                padding: 0,
                selectedColor: Colors.yellow,
                unselectedColor: Colors.cyan,
                roundedEdges: Radius.circular(10),
                selectedGradientColor: LinearGradient(
                  begin: Alignment.topLeft,
                  end: Alignment.bottomRight,
                  colors: [
                    Color.fromRGBO(86, 191, 191, 100),
                    Color.fromRGBO(86, 191, 191, 100)
                  ],
                ),
                unselectedGradientColor: LinearGradient(
                  begin: Alignment.topLeft,
                  end: Alignment.bottomRight,
                  colors: [
                    Color.fromRGBO(204, 204, 204, 100),
                    Color.fromRGBO(204, 204, 204, 100),
                  ],
                ),
              ),
              SizedBox(
                height: 6,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(
                    "100 Donasi",
                    style: TextStyle(
                      fontFamily: "PoppinsRegular",
                      fontSize: 8.0,
                      color: blackColor,
                    ),
                  ),
                  Text(
                    "3 Hari lagi",
                    style: TextStyle(
                      fontFamily: "PoppinsRegular",
                      fontSize: 8.0,
                      color: blackColor,
                    ),
                  ),
                ],
              ),
            ],
          ),
        ),
        margin: EdgeInsets.only(bottom: 3),
      ),
    );
  }
}
