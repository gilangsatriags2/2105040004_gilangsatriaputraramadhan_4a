import 'package:flutter/material.dart';

import '../views/theme.dart';

class RiwayatCampaignDonasi extends StatefulWidget {
  const RiwayatCampaignDonasi({super.key});

  @override
  State<RiwayatCampaignDonasi> createState() => _RiwayatCampaignDonasiState();
}

class _RiwayatCampaignDonasiState extends State<RiwayatCampaignDonasi> {
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(top: 13.0, bottom: 25.0),
      width: 390.0,
      decoration: BoxDecoration(
        color: Color(0x0ffF5F5F5),
        borderRadius: BorderRadius.circular(20),
        boxShadow: [
          BoxShadow(
            color: Color.fromRGBO(0, 0, 0, 0.25),
            spreadRadius: 0,
            blurRadius: 4,
            offset: Offset(4, 4),
          )
        ],
      ),
      child: Padding(
        padding: const EdgeInsets.all(21.0),
        child: Column(
          children: [
            Align(
              alignment: Alignment.topLeft,
              child: Text(
                "Riwayat Campaign Donasi",
                style: TextStyle(
                  fontFamily: "PoppinsSB",
                  fontSize: 15.0,
                  color: blackColor,
                ),
              ),
            ),
            Divider(
              thickness: 2.0,
              color: Color(0x0ffD2D2D2),
            ),
            Container(
              margin: EdgeInsets.only(bottom: 12.0),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(10),
                color: Color(0x0ffFFF2D1),
                border: Border.all(
                  color: Color(0x0ffFBBC1B),
                ),
              ),
              height: 67.0,
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Padding(
                    padding: const EdgeInsets.only(left: 16.0),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Row(
                          children: [
                            Image.asset("images/icons/attention.png"),
                            SizedBox(
                              width: 10,
                            ),
                            Text(
                              "Perhatian",
                              style: TextStyle(
                                fontFamily: "PoppinsRegular",
                                fontSize: 12.0,
                                color: Color(0x0ff535353),
                              ),
                            ),
                          ],
                        ),
                        Text(
                          "Anda belum pernah membuat campaign donasi !!! ",
                          style: TextStyle(
                            fontFamily: "PoppinsRegular",
                            fontSize: 12.0,
                            color: Color(0x0ff535353),
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
