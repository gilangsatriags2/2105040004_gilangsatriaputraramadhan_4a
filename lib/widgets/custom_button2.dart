import 'package:flutter/material.dart';

import '../views/theme.dart';

class CustomButton2 extends StatefulWidget {
  final double width;

  final double height;

  final VoidCallback onpressed;

  final String icon;

  final String saldo;

  const CustomButton2(
      {super.key,
      required this.width,
      required this.height,
      required this.onpressed,
      required this.icon,
      required this.saldo});

  @override
  State<CustomButton2> createState() => _CustomButton2State();
}

class _CustomButton2State extends State<CustomButton2> {
  @override
  Widget build(BuildContext context) {
    return Container(
      width: widget.width,
      height: widget.height,
      decoration: BoxDecoration(
        border: Border.all(
          color: greenColor,
        ),
        borderRadius: BorderRadius.circular(15),
        color: Color(0x0ffDEDEDE),
      ),
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 20.0),
        child: TextButton(
          onPressed: widget.onpressed,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Image.asset("images/icons/${widget.icon}.png"),
              Row(
                children: [
                  Text(
                    "${widget.saldo}",
                    style: TextStyle(
                      fontFamily: "PoppinsLight",
                      fontSize: 13.0,
                      color: blackColor,
                    ),
                  ),
                  Icon(
                    Icons.arrow_forward_ios_rounded,
                    color: greenColor,
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
      margin: EdgeInsets.only(bottom: 10.0),
    );
  }
}
