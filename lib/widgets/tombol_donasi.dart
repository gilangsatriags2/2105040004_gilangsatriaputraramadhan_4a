import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:login_app_hci/views/donasi.dart';
import 'package:login_app_hci/views/theme.dart';

class TombolDonasi extends StatefulWidget {
  const TombolDonasi({super.key});

  @override
  State<TombolDonasi> createState() => _TombolDonasiState();
}

class _TombolDonasiState extends State<TombolDonasi> {
  @override
  Widget build(BuildContext context) {
    return TextButton(
      onPressed: () {
        Navigator.push(
            context, MaterialPageRoute(builder: (context) => DonasiPage()));
      },
      child: Container(
        width: 245.0,
        height: 52.0,
        decoration: BoxDecoration(
            color: greenColor,
            borderRadius: BorderRadius.circular(10.0),
            boxShadow: [
              BoxShadow(
                offset: Offset(4, 4),
                color: Color.fromRGBO(0, 0, 0, 0.25),
                blurRadius: 4,
              )
            ]),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text(
              "DONASI SEKARANG !",
              style: TextStyle(
                fontFamily: "PoppinsMedium",
                fontSize: 20.0,
                color: whiteColor,
              ),
            ),
          ],
        ),
      ),
    );
  }
}
