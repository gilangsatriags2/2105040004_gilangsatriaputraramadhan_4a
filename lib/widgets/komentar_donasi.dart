import 'package:flutter/material.dart';
import 'package:login_app_hci/widgets/kategori.dart';
import '../views/theme.dart';

class KomentarDonasi extends StatefulWidget {
  const KomentarDonasi({super.key});

  @override
  State<KomentarDonasi> createState() => _KomentarDonasiState();
}

class _KomentarDonasiState extends State<KomentarDonasi> {
  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        color: Color(0x0ffCBCBCB),
        borderRadius: BorderRadius.circular(10),
      ),
      width: 360.0,
      child: Padding(
        padding: const EdgeInsets.only(
            top: 12.0, left: 12.0, right: 12.0, bottom: 5.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Row(
              children: [
                CircleAvatar(
                  backgroundImage: NetworkImage(
                    "https://api.multiavatar.com/Starcrasher.png",
                  ),
                ),
                SizedBox(
                  width: 13.0,
                ),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      "Venia Almira",
                      style: TextStyle(
                        fontFamily: "PoppinsMedium",
                        fontSize: 12,
                        color: Color(0x0ff0E8D7E),
                      ),
                    ),
                    Text(
                      "10 menit yang lalu",
                      style: TextStyle(
                        fontFamily: "PoppinsRegular",
                        fontSize: 10.0,
                      ),
                    ),
                  ],
                ),
              ],
            ),
            SizedBox(
              height: 8.0,
            ),
            Text(
              "Semoga donasi yang saya berikan dapat membantu :)",
              style: TextStyle(
                fontFamily: "PoppinsRegular",
                fontSize: 12.0,
              ),
            ),
            SizedBox(
              height: 8.0,
            ),
            RichText(
              text: TextSpan(
                text: "3 orang ",
                style: TextStyle(
                  fontFamily: "PoppinsSB",
                  fontSize: 11.0,
                  color: blackColor,
                ),
                children: [
                  TextSpan(
                    text: "menyukai komentar ini",
                    style: TextStyle(
                      fontFamily: "PoppinsRegular",
                      fontSize: 11,
                      color: blackColor,
                    ),
                  ),
                ],
              ),
            ),
            SizedBox(
              height: 5.0,
            ),
            Divider(
              height: 10.0,
              thickness: 1,
              color: greenColor,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                Kategori(
                  logo: "love",
                  kategori: "Suka",
                  backgroundColor: Colors.transparent,
                  borderColor: Colors.transparent,
                  textColor: blackColor,
                  onpressed: () {},
                  height: 35.0,
                ),
                Kategori(
                  logo: "berbagi",
                  kategori: "Bagikan",
                  backgroundColor: Colors.transparent,
                  borderColor: Colors.transparent,
                  textColor: blackColor,
                  onpressed: () {},
                  height: 35.0,
                ),
              ],
            ),
          ],
        ),
      ),
      margin: EdgeInsets.only(bottom: 10.0),
    );
  }
}
