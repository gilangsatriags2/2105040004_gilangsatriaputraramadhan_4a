import 'package:flutter/material.dart';
import 'package:login_app_hci/views/theme.dart';

class CustomTextField extends StatefulWidget {
  final double width;
  final double height;
  final Color color;
  final String hintText;
  const CustomTextField(
      {super.key,
      required this.width,
      required this.height,
      required this.color,
      required this.hintText});

  @override
  State<CustomTextField> createState() => _CustomTextFieldState();
}

class _CustomTextFieldState extends State<CustomTextField> {
  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(15),
        color: widget.color,
      ),
      width: widget.width,
      child: TextField(
        keyboardType: TextInputType.multiline, // Mengaktifkan input multiline
        textInputAction: TextInputAction.newline,
        decoration: InputDecoration(
          hintText: widget.hintText,
          hintStyle: TextStyle(fontFamily: "PoppinsLight", fontSize: 13.0),
          focusedBorder: OutlineInputBorder(
            borderRadius: BorderRadius.all(Radius.circular(15)),
            borderSide: BorderSide(
              width: 1,
              color: greenColor,
            ),
          ),
          enabledBorder: OutlineInputBorder(
            borderRadius: BorderRadius.all(Radius.circular(15)),
            borderSide: BorderSide(
              width: 1,
              color: greenColor,
            ),
          ),
        ),
      ),
    );
  }
}
