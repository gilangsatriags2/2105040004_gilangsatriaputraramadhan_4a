import 'package:dropdown_button2/dropdown_button2.dart';
import 'package:flutter/material.dart';
import 'package:login_app_hci/views/theme.dart';

import 'custom_dropdown.dart';

class Dropdown extends StatefulWidget {
  const Dropdown({super.key});

  @override
  State<Dropdown> createState() => _DropdownState();
}

class _DropdownState extends State<Dropdown> {
  final List<String> kategori = [
    'Disabilitas',
    'Lansia',
    'Fakir Miskin',
    'Anak Jalanan',
  ];
  String? selectedValue;
  @override
  Widget build(BuildContext context) {
    return CustomDropdownButton2(
      hint: 'Semua',
      dropdownItems: kategori,
      value: selectedValue,
      buttonDecoration: BoxDecoration(
        borderRadius: BorderRadius.circular(25),
        border: Border.all(
          color: greenColor,
        ),
        color: greyColor,
      ),
      buttonHeight: 40.0,
      buttonWidth: 159.0,
      dropdownWidth: 159.0,
      onChanged: (value) {
        setState(() {
          selectedValue = value;
        });
      },
    );
  }
}
