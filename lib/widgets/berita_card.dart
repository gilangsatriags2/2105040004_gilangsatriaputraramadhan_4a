import 'package:flutter/material.dart';
import 'package:login_app_hci/views/theme.dart';

class BeritaCard extends StatefulWidget {
  const BeritaCard({super.key, required this.text});
  final String text;

  @override
  State<BeritaCard> createState() => _BeritaCardState();
}

class _BeritaCardState extends State<BeritaCard> {
  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
          border: Border.all(color: Color(0x0ff56BFBF)),
          color: Color(0x0ffF5F5F5),
          borderRadius: BorderRadius.circular(20)),
      child: Padding(
        padding: const EdgeInsets.all(10.0),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Image.asset("images/smile.png"),
            SizedBox(
              width: 18.0,
            ),
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  "Charity",
                  style: TextStyle(
                    fontFamily: "PoppinsMedium",
                    fontSize: 18.0,
                    color: blackColor,
                  ),
                ),
                SizedBox(
                  height: 2.0,
                ),
                Container(
                  width: 280,
                  child: Text(
                    "${widget.text}",
                    style: TextStyle(
                      fontFamily: "PoppinsRegular",
                      fontSize: 12.0,
                      color: blackColor,
                    ),
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
      margin: EdgeInsets.only(top: 12.0),
    );
  }
}
