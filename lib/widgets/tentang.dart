import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import '../views/theme.dart';

class Tentang extends StatefulWidget {
  const Tentang(
      {super.key,
      required this.email,
      required this.phone,
      required this.bank,
      required this.noRek,
      required this.noAkun,
      required this.onpressed});
  final String email;
  final String phone;
  final String bank;
  final String noRek;
  final int noAkun;
  final VoidCallback onpressed;

  @override
  State<Tentang> createState() => _TentangState();
}

class _TentangState extends State<Tentang> {
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(top: 13.0),
      width: 390.0,
      height: 250.0,
      decoration: BoxDecoration(
        color: Color(0x0ffF5F5F5),
        borderRadius: BorderRadius.circular(20),
        boxShadow: [
          BoxShadow(
            color: Color.fromRGBO(0, 0, 0, 0.25),
            spreadRadius: 0,
            blurRadius: 4,
            offset: Offset(4, 4),
          )
        ],
      ),
      child: Padding(
        padding: const EdgeInsets.all(19.0),
        child: Column(
          children: [
            Align(
              alignment: Alignment.topLeft,
              child: Text(
                "Tentang",
                style: TextStyle(
                  fontFamily: "PoppinsSB",
                  fontSize: 15.0,
                  color: blackColor,
                ),
              ),
            ),
            Divider(
              thickness: 2.0,
              color: Color(0x0ffD2D2D2),
            ),
            Wrap(
              spacing: 10.0, // Jarak horizontal antara elemen-elemen anak
              runSpacing: 10.0,
              children: <Widget>[
                Row(
                  children: [
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Text(
                          "Bank",
                          style: TextStyle(
                            fontFamily: "PoppinsLight",
                            fontSize: 14.0,
                            color: Color(0x0ff787878),
                          ),
                        ),
                        widget.bank == "null"
                            ? Text(
                                "-",
                                style: TextStyle(
                                  fontFamily: "PoppinsRegular",
                                  fontSize: 14.0,
                                  color: Color(0x0ff444444),
                                ),
                              )
                            : Text(
                                "${widget.bank}",
                                style: TextStyle(
                                  fontFamily: "PoppinsRegular",
                                  fontSize: 14.0,
                                  color: Color(0x0ff444444),
                                ),
                              ),
                      ],
                    ),
                    SizedBox(
                      width: 140,
                    ),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Text(
                          "Telepon",
                          style: TextStyle(
                            fontFamily: "PoppinsLight",
                            fontSize: 14.0,
                            color: Color(0x0ff787878),
                          ),
                        ),
                        Text(
                          "${widget.phone}",
                          style: TextStyle(
                            fontFamily: "PoppinsRegular",
                            fontSize: 14.0,
                            color: Color(0x0ff444444),
                          ),
                        )
                      ],
                    ),
                  ],
                ),
                Row(
                  children: [
                    Container(
                      width: 65,
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Text(
                            "Rekening",
                            style: TextStyle(
                              fontFamily: "PoppinsLight",
                              fontSize: 14.0,
                              color: Color(0x0ff787878),
                            ),
                          ),
                          widget.noRek == "null"
                              ? Text(
                                  "-",
                                  style: TextStyle(
                                    fontFamily: "PoppinsRegular",
                                    fontSize: 14.0,
                                    color: Color(0x0ff444444),
                                  ),
                                )
                              : Text(
                                  "${widget.noRek}",
                                  style: TextStyle(
                                    fontFamily: "PoppinsRegular",
                                    fontSize: 14.0,
                                    color: Color(0x0ff444444),
                                  ),
                                ),
                        ],
                      ),
                    ),
                    SizedBox(
                      width: 110,
                    ),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Text(
                          "ID Akun",
                          style: TextStyle(
                            fontFamily: "PoppinsLight",
                            fontSize: 14.0,
                            color: Color(0x0ff787878),
                          ),
                        ),
                        widget.noAkun.toString() == "null"
                            ? Text(
                                "-",
                                style: TextStyle(
                                  fontFamily: "PoppinsRegular",
                                  fontSize: 14.0,
                                  color: Color(0x0ff444444),
                                ),
                              )
                            : Text(
                                "#${widget.noAkun.toString()}",
                                style: TextStyle(
                                  fontFamily: "PoppinsRegular",
                                  fontSize: 14.0,
                                  color: Color(0x0ff444444),
                                ),
                              ),
                      ],
                    ),
                  ],
                ),
                Row(
                  children: [
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Text(
                          "Email",
                          style: TextStyle(
                            fontFamily: "PoppinsLight",
                            fontSize: 14.0,
                            color: Color(0x0ff787878),
                          ),
                        ),
                        Text(
                          "${widget.email}",
                          style: TextStyle(
                            fontFamily: "PoppinsRegular",
                            fontSize: 14.0,
                            color: Color(0x0ff444444),
                          ),
                        ),
                      ],
                    ),
                    SizedBox(
                      width: 20,
                    ),
                    Container(
                      width: 100,
                      height: 35,
                      decoration: BoxDecoration(
                        color: greenColor,
                        borderRadius: BorderRadius.circular(10.0),
                      ),
                      child: TextButton(
                        onPressed: widget.onpressed,
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            Image.asset("images/icons/edit.png"),
                            SizedBox(
                              width: 10.0,
                            ),
                            Text(
                              "Edit",
                              style: TextStyle(
                                fontFamily: "PoppinsMedium",
                                fontSize: 16.0,
                                color: whiteColor,
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
