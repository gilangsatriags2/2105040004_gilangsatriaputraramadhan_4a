import 'package:flutter/material.dart';

import '../views/theme.dart';

class Pendonasi extends StatefulWidget {
  const Pendonasi({super.key});

  @override
  State<Pendonasi> createState() => _PendonasiState();
}

class _PendonasiState extends State<Pendonasi> {
  @override
  Widget build(BuildContext context) {
    return Container(
      width: 350.0,
      height: 82.0,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(10),
        color: Color(0x0ffcbcbcb),
      ),
      child: Padding(
        padding: const EdgeInsets.only(left: 16.0, right: 16.0),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            CircleAvatar(
              backgroundImage: NetworkImage(
                "https://api.multiavatar.com/Starcrasher.png",
              ),
            ),
            SizedBox(
              width: 17,
            ),
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text(
                  "Venia Almira",
                  style: TextStyle(
                    fontFamily: "PoppinsMedium",
                    fontSize: 12,
                    color: Color(0x0ff0E8D7E),
                  ),
                ),
                RichText(
                  text: TextSpan(
                    text: "Donasi ",
                    style: TextStyle(
                      fontFamily: "PoppinsRegular",
                      fontSize: 11.0,
                      color: blackColor,
                    ),
                    children: [
                      TextSpan(
                        text: "Rp. 100.000,00",
                        style: TextStyle(
                          fontFamily: "PoppinsSB",
                          fontSize: 11,
                          color: blackColor,
                        ),
                      ),
                    ],
                  ),
                ),
                Text(
                  "10 menit yang lalu",
                  style: TextStyle(
                    fontFamily: "PoppinsRegular",
                    fontSize: 10.0,
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
      margin: EdgeInsets.only(bottom: 10.0),
    );
  }
}
