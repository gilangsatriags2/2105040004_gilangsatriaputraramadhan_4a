import 'package:flutter/material.dart';

import '../views/theme.dart';

class RiwayatDonasi extends StatefulWidget {
  const RiwayatDonasi({super.key});

  @override
  State<RiwayatDonasi> createState() => _RiwayatDonasiState();
}

class _RiwayatDonasiState extends State<RiwayatDonasi> {
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(top: 13.0, bottom: 8.0),
      width: 390.0,
      decoration: BoxDecoration(
        color: Color(0x0ffF5F5F5),
        borderRadius: BorderRadius.circular(20),
        boxShadow: [
          BoxShadow(
            color: Color.fromRGBO(0, 0, 0, 0.25),
            spreadRadius: 0,
            blurRadius: 4,
            offset: Offset(4, 4),
          )
        ],
      ),
      child: Padding(
        padding: const EdgeInsets.all(21.0),
        child: Column(
          children: [
            Align(
              alignment: Alignment.topLeft,
              child: Text(
                "Riwayat Donasi",
                style: TextStyle(
                  fontFamily: "PoppinsSB",
                  fontSize: 15.0,
                  color: blackColor,
                ),
              ),
            ),
            Divider(
              thickness: 2.0,
              color: Color(0x0ffD2D2D2),
            ),
            Container(
              margin: EdgeInsets.only(bottom: 12.0),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(10),
                color: Color(0x0ffE7E7E7),
              ),
              height: 55.0,
              child: Padding(
                padding: const EdgeInsets.all(10.0),
                child: RichText(
                  textAlign: TextAlign.justify,
                  text: TextSpan(
                    text: "Berdonasi ",
                    style: TextStyle(
                      fontFamily: "PoppinsRegular",
                      fontSize: 12.0,
                      color: blackColor,
                    ),
                    children: [
                      TextSpan(
                        text: "Rp. 100.000,00 ",
                        style: TextStyle(
                          fontFamily: "PoppinsSB",
                          fontSize: 12,
                          color: greenColor,
                        ),
                      ),
                      TextSpan(
                        text: "pada campaign berjudul ",
                        style: TextStyle(
                          fontFamily: "PoppinsRegular",
                          fontSize: 12.0,
                          color: blackColor,
                        ),
                      ),
                      TextSpan(
                        text:
                            "“Bantu kakek paul untuk mendapatkan tempat tinggal”",
                        style: TextStyle(
                          fontFamily: "PoppinsSB",
                          fontSize: 12.0,
                          color: blackColor,
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ),
            Container(
              margin: EdgeInsets.only(bottom: 12.0),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(10),
                color: Color(0x0ffE7E7E7),
              ),
              height: 55.0,
              child: Padding(
                padding: const EdgeInsets.all(10.0),
                child: RichText(
                  textAlign: TextAlign.justify,
                  text: TextSpan(
                    text: "Berdonasi ",
                    style: TextStyle(
                      fontFamily: "PoppinsRegular",
                      fontSize: 12.0,
                      color: blackColor,
                    ),
                    children: [
                      TextSpan(
                        text: "Rp. 50.000,00 ",
                        style: TextStyle(
                          fontFamily: "PoppinsSB",
                          fontSize: 12,
                          color: greenColor,
                        ),
                      ),
                      TextSpan(
                        text: "pada campaign berjudul ",
                        style: TextStyle(
                          fontFamily: "PoppinsRegular",
                          fontSize: 12.0,
                          color: blackColor,
                        ),
                      ),
                      TextSpan(
                        text:
                            "““Banyak anak kecil terlantar di Bandung, Ayo bantu !!!!!””",
                        style: TextStyle(
                          fontFamily: "PoppinsSB",
                          fontSize: 12.0,
                          color: blackColor,
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ),
            Container(
              height: 35.0,
              width: 150.0,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(10),
                color: greenColor,
              ),
              child: Padding(
                padding: const EdgeInsets.symmetric(
                  vertical: 2.0,
                  horizontal: 6.0,
                ),
                child: TextButton(
                  onPressed: () {},
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text(
                        "Lihat selengkapnya",
                        style: TextStyle(
                          fontFamily: "PoppinsRegular",
                          fontSize: 10,
                          color: whiteColor,
                        ),
                      ),
                      SizedBox(
                        width: 5.0,
                      ),
                      Image.asset(
                        "images/icons/arrow_bottom.png",
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
