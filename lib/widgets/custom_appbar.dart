import 'package:flutter/material.dart';

import '../views/theme.dart';

class CustomAppBar extends StatelessWidget implements PreferredSizeWidget {
  final String title;
  final VoidCallback onBackButtonPressed;

  const CustomAppBar({
    Key? key,
    required this.title,
    required this.onBackButtonPressed,
  }) : super(key: key);

  @override
  Size get preferredSize => Size.fromHeight(kToolbarHeight);

  @override
  Widget build(BuildContext context) {
    return AppBar(
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20)),
      title: Container(
        child: Align(
          alignment: Alignment.centerLeft,
          child: Text(
            "${title}",
            style: TextStyle(
              fontFamily: "PoppinsRegular",
              fontSize: 15.0,
              color: blackColor,
            ),
          ),
        ),
      ),
      leading: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Container(
          decoration: BoxDecoration(
              color: Color(0x0ff54C1C1),
              borderRadius: BorderRadius.circular(100)),
          child: IconButton(
            onPressed: onBackButtonPressed,
            icon: Icon(Icons.arrow_back_ios_new_rounded),
            color: blackColor,
          ),
        ),
      ),
      backgroundColor: Color(0x0ffEBEBEB),
      elevation: 0,
    );
  }
}
