import 'package:flutter/material.dart';
import 'package:login_app_hci/views/theme.dart';

class RadioButton extends StatefulWidget {
  final String nominal;
  final String title;

  const RadioButton({
    super.key,
    required this.nominal,
    required this.title,
  });

  @override
  State<RadioButton> createState() => _RadioButtonState();
}

class _RadioButtonState extends State<RadioButton> {
  String? valueNominal;
  @override
  Widget build(BuildContext context) {
    return Container(
      width: 180.0,
      decoration: BoxDecoration(
          border: Border.all(
            color: greenColor,
          ),
          color: Color(0x0ffE9E9E9),
          borderRadius: BorderRadius.circular(15.0)),
      child: RadioListTile(
        activeColor: greenColor,
        title: Text(
          "${widget.title}",
          style: TextStyle(
            fontFamily: "PoppinsLight",
            fontSize: 13.0,
            color: blackColor,
          ),
        ),
        value: widget.nominal,
        groupValue: valueNominal,
        onChanged: (value) {
          setState(() {
            valueNominal = value.toString();
          });
        },
      ),
    );
  }
}
