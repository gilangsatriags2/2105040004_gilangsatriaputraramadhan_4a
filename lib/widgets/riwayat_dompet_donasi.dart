import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import '../views/theme.dart';

class RiwayatDompetDonasi extends StatefulWidget {
  const RiwayatDompetDonasi({super.key});

  @override
  State<RiwayatDompetDonasi> createState() => _RiwayatDompetDonasiState();
}

class _RiwayatDompetDonasiState extends State<RiwayatDompetDonasi> {
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(top: 13.0, bottom: 8.0),
      width: 390.0,
      decoration: BoxDecoration(
        color: Color(0x0ffF5F5F5),
        borderRadius: BorderRadius.circular(20),
        boxShadow: [
          BoxShadow(
            color: Color.fromRGBO(0, 0, 0, 0.25),
            spreadRadius: 0,
            blurRadius: 4,
            offset: Offset(4, 4),
          )
        ],
      ),
      child: Padding(
        padding: const EdgeInsets.all(21.0),
        child: Column(
          children: [
            Align(
              alignment: Alignment.topLeft,
              child: Text(
                "Riwayat Dompet Donasi",
                style: TextStyle(
                  fontFamily: "PoppinsSB",
                  fontSize: 15.0,
                  color: blackColor,
                ),
              ),
            ),
            Divider(
              thickness: 2.0,
              color: Color(0x0ffD2D2D2),
            ),
            Container(
              margin: EdgeInsets.only(bottom: 12.0),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(10),
                color: Color(0x0ffE7E7E7),
              ),
              height: 55.0,
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  SizedBox(
                    width: 19.0,
                  ),
                  Image.asset("images/icons/topup.png"),
                  SizedBox(
                    width: 17.0,
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      RichText(
                        text: TextSpan(
                          text: "Isi dompet ",
                          style: TextStyle(
                            fontFamily: "PoppinsRegular",
                            fontSize: 12.0,
                            color: blackColor,
                          ),
                          children: [
                            TextSpan(
                              text: "Rp. 100.000,00",
                              style: TextStyle(
                                fontFamily: "PoppinsSB",
                                fontSize: 12,
                                color: blackColor,
                              ),
                            ),
                          ],
                        ),
                      ),
                      Text(
                        "05 Desember 2022, 23:22 WIB",
                        style: TextStyle(
                          fontFamily: "PoppinsRegular",
                          fontSize: 12.0,
                          color: blackColor,
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            ),
            Container(
              margin: EdgeInsets.only(bottom: 12.0),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(10),
                color: Color(0x0ffE7E7E7),
              ),
              height: 55.0,
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  SizedBox(
                    width: 19.0,
                  ),
                  Image.asset("images/icons/red_withdraw.png"),
                  SizedBox(
                    width: 17.0,
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      RichText(
                        text: TextSpan(
                          text: "Isi berkurang dompet ",
                          style: TextStyle(
                            fontFamily: "PoppinsRegular",
                            fontSize: 12.0,
                            color: blackColor,
                          ),
                          children: [
                            TextSpan(
                              text: "Rp. 50.000,00",
                              style: TextStyle(
                                fontFamily: "PoppinsSB",
                                fontSize: 12,
                                color: blackColor,
                              ),
                            ),
                          ],
                        ),
                      ),
                      Text(
                        "04 Desember 2022, 21:22 WIB",
                        style: TextStyle(
                          fontFamily: "PoppinsRegular",
                          fontSize: 12.0,
                          color: blackColor,
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            ),
            Container(
              height: 35.0,
              width: 150.0,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(10),
                color: greenColor,
              ),
              child: Padding(
                padding: const EdgeInsets.symmetric(
                  vertical: 2.0,
                  horizontal: 6.0,
                ),
                child: TextButton(
                  onPressed: () {},
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text(
                        "Lihat selengkapnya",
                        style: TextStyle(
                          fontFamily: "PoppinsRegular",
                          fontSize: 10,
                          color: whiteColor,
                        ),
                      ),
                      SizedBox(
                        width: 5.0,
                      ),
                      Image.asset(
                        "images/icons/arrow_bottom.png",
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
