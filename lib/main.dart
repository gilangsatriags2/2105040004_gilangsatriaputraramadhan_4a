// import 'package:flutter/material.dart';
// import 'package:firebase_core/firebase_core.dart';
// import 'package:firebase_auth/firebase_auth.dart';
// import 'package:login_app_hci/firebase_options.dart';
// import 'package:login_app_hci/widgets/bottom_navbar.dart';
// import 'views/login.dart';

// void main() async {
//   WidgetsFlutterBinding.ensureInitialized();

//   await Firebase.initializeApp(
//     options: DefaultFirebaseOptions.currentPlatform,
//   );

//   runApp(const MyApp());
// }

// class MyApp extends StatelessWidget {
//   const MyApp({super.key});

//   // This widget is the root of your application.
//   @override
//   Widget build(BuildContext context) {
//     return MaterialApp(
//       debugShowCheckedModeBanner: false,
//       title: 'Flutter Demo',
//       theme: ThemeData(
//         // This is the theme of your application.
//         //
//         // Try running your application with "flutter run". You'll see the
//         // application has a blue toolbar. Then, without quitting the app, try
//         // changing the primarySwatch below to Colors.green and then invoke
//         // "hot reload" (press "r" in the console where you ran "flutter run",
//         // or simply save your changes to "hot reload" in a Flutter IDE).
//         // Notice that the counter didn't reset back to zero; the application
//         // is not restarted.
//         primarySwatch: Colors.blue,
//       ),
//       home: StreamBuilder<User?>(
//         stream: FirebaseAuth.instance.userChanges(),
//         builder: (context, snapshot) {
//           String? documentId;

//           if (snapshot.hasData) {
//             documentId = snapshot.data!.uid;
//             return BottomNavbar(
//                 // docId: documentId,
//                 );
//           } else {
//             return LoginPage();
//           }
//         },
//       ),
//     );
//   }
// }
import 'package:flutter/material.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:login_app_hci/service/DocIdProvider.dart';
import 'package:provider/provider.dart'; // Import provider package
import 'package:login_app_hci/firebase_options.dart';
import 'package:login_app_hci/widgets/bottom_navbar.dart'; // Import DocIdProvider
import 'views/login.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();

  await Firebase.initializeApp(
    options: DefaultFirebaseOptions.currentPlatform,
  );

  runApp(
    ChangeNotifierProvider(
      create: (context) => DocIdProvider(), // Provide DocIdProvider
      child: const MyApp(),
    ),
  );
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: StreamBuilder<User?>(
        stream: FirebaseAuth.instance.userChanges(),
        builder: (context, snapshot) {
          final docIdProvider = Provider.of<DocIdProvider>(context,
              listen: false); // Access DocIdProvider

          if (snapshot.hasData) {
            final email = snapshot.data!.email;
            docIdProvider.fetchDocId(email!); // Fetch the docId
            return Consumer<DocIdProvider>(
              builder: (context, provider, _) {
                final docId = provider.docId;
                return BottomNavbar(docId: docId); // Pass docId to BottomNavbar
              },
            );
          } else {
            return LoginPage();
          }
        },
      ),
    );
  }
}
