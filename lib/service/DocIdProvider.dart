import 'package:flutter/foundation.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

class DocIdProvider extends ChangeNotifier {
  String? _docId;

  String? get docId => _docId;

  Future<void> fetchDocId(String email) async {
    try {
      QuerySnapshot querySnapshot = await FirebaseFirestore.instance
          .collection('users')
          .where('email', isEqualTo: email)
          .limit(1)
          .get();
      if (querySnapshot.size > 0) {
        _docId = querySnapshot.docs[0].id;
        notifyListeners();
      } else {
        print('No document found for the logged-in email.');
      }
    } catch (error) {
      print('Error fetching docId: $error');
    }
  }
}
