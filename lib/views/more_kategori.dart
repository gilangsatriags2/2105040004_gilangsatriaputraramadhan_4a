import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:login_app_hci/views/theme.dart';
import 'package:login_app_hci/widgets/custom_appbar.dart';
import 'package:login_app_hci/widgets/kategori.dart';

class MoreKategoriPage extends StatefulWidget {
  const MoreKategoriPage({super.key});

  @override
  State<MoreKategoriPage> createState() => _MoreKategoriPageState();
}

class _MoreKategoriPageState extends State<MoreKategoriPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: CustomAppBar(
          title: "Kategori",
          onBackButtonPressed: () {
            Navigator.pop(context);
          }),
      body: ListView(
        children: [
          Padding(
            padding: const EdgeInsets.only(top: 17.0, left: 24.0, right: 47.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  "Favorit Kamu",
                  style: TextStyle(
                    fontFamily: "PoppinsMedium",
                    fontSize: 16.0,
                  ),
                ),
                SizedBox(
                  height: 12.0,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Kategori(
                      logo: "mountain",
                      kategori: "Bencana Alam",
                      backgroundColor: whiteColor,
                      borderColor: greenColor,
                      textColor: blackColor,
                      onpressed: () {},
                      height: 33.0,
                    ),
                    Kategori(
                      logo: "kesehatan",
                      kategori: "Kesehatan",
                      backgroundColor: whiteColor,
                      borderColor: greenColor,
                      textColor: blackColor,
                      onpressed: () {},
                      height: 33.0,
                    ),
                    Kategori(
                      logo: "sosial",
                      kategori: "Sosial",
                      backgroundColor: whiteColor,
                      borderColor: greenColor,
                      textColor: blackColor,
                      onpressed: () {},
                      height: 33.0,
                    ),
                  ],
                ),
                SizedBox(
                  height: 12.0,
                ),
                Text(
                  "Kategori Lain",
                  style: TextStyle(
                    fontFamily: "PoppinsMedium",
                    fontSize: 16.0,
                  ),
                ),
                SizedBox(
                  height: 12.0,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Kategori(
                      logo: "kemanusiaan",
                      kategori: "Kemanusiaan",
                      backgroundColor: whiteColor,
                      borderColor: greenColor,
                      textColor: blackColor,
                      onpressed: () {},
                      height: 33.0,
                    ),
                    Kategori(
                      logo: "ekonomi",
                      kategori: "Ekonomi",
                      backgroundColor: whiteColor,
                      borderColor: greenColor,
                      textColor: blackColor,
                      onpressed: () {},
                      height: 33.0,
                    ),
                    Kategori(
                      logo: "pohon",
                      kategori: "Lingkungan",
                      backgroundColor: whiteColor,
                      borderColor: greenColor,
                      textColor: blackColor,
                      onpressed: () {},
                      height: 33.0,
                    ),
                  ],
                ),
                SizedBox(
                  height: 12.0,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Kategori(
                      logo: "pendidikan",
                      kategori: "Pendidikan",
                      backgroundColor: whiteColor,
                      borderColor: greenColor,
                      textColor: blackColor,
                      onpressed: () {},
                      height: 33.0,
                    ),
                    Kategori(
                      logo: "difabel",
                      kategori: "Difabel",
                      backgroundColor: whiteColor,
                      borderColor: greenColor,
                      textColor: blackColor,
                      onpressed: () {},
                      height: 33.0,
                    ),
                    Kategori(
                      logo: "panti",
                      kategori: "Panti Asuhan",
                      backgroundColor: whiteColor,
                      borderColor: greenColor,
                      textColor: blackColor,
                      onpressed: () {},
                      height: 33.0,
                    ),
                  ],
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
