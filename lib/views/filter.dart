import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:login_app_hci/views/theme.dart';
import 'package:login_app_hci/widgets/custom_appbar.dart';
import 'package:login_app_hci/widgets/kategori.dart';

class FilterPage extends StatefulWidget {
  const FilterPage({super.key});

  @override
  State<FilterPage> createState() => _FilterPageState();
}

class _FilterPageState extends State<FilterPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: CustomAppBar(
        title: "Filter",
        onBackButtonPressed: () {
          Navigator.pop(context);
        },
      ),
      body: ListView(
        children: [
          Column(
            children: [
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 31.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      "Lokasi",
                      style: TextStyle(
                        fontFamily: "PoppinsRegular",
                        fontSize: 15.0,
                      ),
                    ),
                    TextButton(
                      onPressed: () {},
                      child: Text(
                        "Lihat lainnya",
                        style: TextStyle(
                          fontFamily: "PoppinsMedium",
                          fontSize: 13.0,
                          color: greenColor,
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(left: 31.0, right: 52),
                child: Column(
                  children: [
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Kategori(
                          logo: null,
                          kategori: "Kota Magelang",
                          backgroundColor: whiteColor,
                          borderColor: greenColor,
                          textColor: blackColor,
                          onpressed: () {},
                          height: 35.0,
                        ),
                        Kategori(
                          logo: null,
                          kategori: "Kota Yogyakarta",
                          backgroundColor: whiteColor,
                          borderColor: greenColor,
                          textColor: blackColor,
                          onpressed: () {},
                          height: 35.0,
                        ),
                        Kategori(
                          logo: null,
                          kategori: "Kota Bali",
                          backgroundColor: whiteColor,
                          borderColor: greenColor,
                          textColor: blackColor,
                          onpressed: () {},
                          height: 35.0,
                        ),
                      ],
                    ),
                  ],
                ),
              ),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 31.0),
                child: Divider(
                  height: 30.0,
                  thickness: 2,
                ),
              ),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 31.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      "Kategori",
                      style: TextStyle(
                        fontFamily: "PoppinsRegular",
                        fontSize: 15.0,
                      ),
                    ),
                    TextButton(
                      onPressed: () {},
                      child: Text(
                        "Lihat lainnya",
                        style: TextStyle(
                          fontFamily: "PoppinsMedium",
                          fontSize: 13.0,
                          color: greenColor,
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(left: 31.0, right: 52),
                child: Column(
                  children: [
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Kategori(
                          onpressed: () {},
                          height: 35.0,
                          backgroundColor: Colors.transparent,
                          borderColor: greenColor,
                          logo: "mountain",
                          textColor: null,
                          kategori: "Bencana Alam",
                        ),
                        SizedBox(
                          width: 5.0,
                        ),
                        Kategori(
                          onpressed: () {},
                          height: 35.0,
                          backgroundColor: Colors.transparent,
                          borderColor: greenColor,
                          logo: "kesehatan",
                          textColor: null,
                          kategori: "Kesehatan",
                        ),
                        SizedBox(
                          width: 5.0,
                        ),
                        Kategori(
                          onpressed: () {},
                          height: 35.0,
                          backgroundColor: Colors.transparent,
                          borderColor: greenColor,
                          logo: "sosial",
                          textColor: null,
                          kategori: "Sosial",
                        ),
                      ],
                    ),
                  ],
                ),
              ),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 31.0),
                child: Divider(
                  height: 30.0,
                  thickness: 2,
                ),
              ),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 31.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      "Jarak",
                      style: TextStyle(
                        fontFamily: "PoppinsRegular",
                        fontSize: 15.0,
                      ),
                    ),
                  ],
                ),
              ),
              SizedBox(
                height: 10.0,
              ),
              Padding(
                padding: const EdgeInsets.only(left: 31.0, right: 52),
                child: Column(
                  children: [
                    Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        Kategori(
                          logo: null,
                          kategori: "Terdekat",
                          backgroundColor: whiteColor,
                          borderColor: greenColor,
                          textColor: blackColor,
                          onpressed: () {},
                          height: 35.0,
                        ),
                        SizedBox(
                          width: 20.0,
                        ),
                        Kategori(
                          logo: null,
                          kategori: "Terjauh",
                          backgroundColor: whiteColor,
                          borderColor: greenColor,
                          textColor: blackColor,
                          onpressed: () {},
                          height: 35.0,
                        ),
                      ],
                    ),
                  ],
                ),
              ),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 31.0),
                child: Divider(
                  height: 30.0,
                  thickness: 2,
                ),
              ),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 31.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      "Jumlah Pendonasi",
                      style: TextStyle(
                        fontFamily: "PoppinsRegular",
                        fontSize: 15.0,
                      ),
                    ),
                  ],
                ),
              ),
              SizedBox(
                height: 10.0,
              ),
              Padding(
                padding: const EdgeInsets.only(left: 31.0, right: 52),
                child: Column(
                  children: [
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Kategori(
                          logo: null,
                          kategori: "Paling banyak donasi",
                          backgroundColor: whiteColor,
                          borderColor: greenColor,
                          textColor: blackColor,
                          onpressed: () {},
                          height: 35.0,
                        ),
                        Kategori(
                          logo: null,
                          kategori: "Paling sedikit donasi",
                          backgroundColor: whiteColor,
                          borderColor: greenColor,
                          textColor: blackColor,
                          onpressed: () {},
                          height: 35.0,
                        ),
                      ],
                    ),
                  ],
                ),
              ),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 31.0),
                child: Divider(
                  height: 30.0,
                  thickness: 2,
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }
}
