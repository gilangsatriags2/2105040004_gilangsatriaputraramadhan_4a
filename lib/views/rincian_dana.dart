import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:login_app_hci/views/theme.dart';
import 'package:login_app_hci/widgets/custom_appbar.dart';

class RincianDanaPage extends StatefulWidget {
  const RincianDanaPage({super.key});

  @override
  State<RincianDanaPage> createState() => _RincianDanaPageState();
}

class _RincianDanaPageState extends State<RincianDanaPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: whiteColor,
      appBar: CustomAppBar(
          title: "Rincian Dana Donasi",
          onBackButtonPressed: () {
            Navigator.pop(context);
          }),
      body: ListView(
        children: [
          Column(
            children: [
              Padding(
                padding: EdgeInsets.symmetric(vertical: 17.0, horizontal: 36.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Image.asset("images/icons/dompet.png"),
                    SizedBox(
                      width: 14.0,
                    ),
                    Text(
                      "Status Dana Terkumpul",
                      style: TextStyle(
                        fontFamily: "PoppinsMedium",
                        fontSize: 15.0,
                        color: blackColor,
                      ),
                    ),
                  ],
                ),
              ),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 19.0),
                child: Container(
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(10),
                    color: Color(0x0ffF5F5F5),
                  ),
                  child: Padding(
                    padding: const EdgeInsets.only(
                        top: 16.0, bottom: 16.0, right: 15.0, left: 16.0),
                    child: Column(
                      children: [
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Text(
                              "Dana Terkumpul",
                              style: TextStyle(
                                fontFamily: "PoppinsRegular",
                                fontSize: 14.0,
                                color: blackColor,
                              ),
                            ),
                            Text(
                              "Rp. 5.000.000,00",
                              style: TextStyle(
                                fontFamily: "PoppinsMedium",
                                fontSize: 14.0,
                                color: blackColor,
                              ),
                            ),
                          ],
                        ),
                        SizedBox(
                          height: 10.0,
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Text(
                              "Dana sudah dicairkan *",
                              style: TextStyle(
                                fontFamily: "PoppinsRegular",
                                fontSize: 14.0,
                                color: blackColor,
                              ),
                            ),
                            Text(
                              "Rp. 1.000.000,00",
                              style: TextStyle(
                                fontFamily: "PoppinsMedium",
                                fontSize: 14.0,
                                color: blackColor,
                              ),
                            ),
                          ],
                        ),
                        SizedBox(
                          height: 10.0,
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Text(
                              "Dana operasional Charity **",
                              style: TextStyle(
                                fontFamily: "PoppinsRegular",
                                fontSize: 14.0,
                                color: blackColor,
                              ),
                            ),
                            Text(
                              "Rp. 250.000,00",
                              style: TextStyle(
                                fontFamily: "PoppinsMedium",
                                fontSize: 14.0,
                                color: blackColor,
                              ),
                            ),
                          ],
                        ),
                        SizedBox(
                          height: 10.0,
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Text(
                              "Dana Tersisa",
                              style: TextStyle(
                                fontFamily: "PoppinsRegular",
                                fontSize: 14.0,
                                color: blackColor,
                              ),
                            ),
                            Text(
                              "Rp. 3.750.000,00",
                              style: TextStyle(
                                fontFamily: "PoppinsMedium",
                                fontSize: 14.0,
                                color: blackColor,
                              ),
                            ),
                          ],
                        ),
                        SizedBox(
                          height: 10.0,
                        ),
                        Container(
                          decoration: BoxDecoration(
                              color: greenColor,
                              borderRadius: BorderRadius.circular(10.0)),
                          padding: EdgeInsets.only(
                            left: 23.0,
                            top: 11.0,
                            bottom: 11.0,
                            right: 6.0,
                          ),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Icon(
                                Icons.timer,
                                color: whiteColor,
                              ),
                              SizedBox(
                                width: 21.0,
                              ),
                              Container(
                                width: 268,
                                child: Text(
                                  "Data diperbarui setiap 10 menit. Terakhir diperbarui pada 04-Des-2022 21:25 WIB",
                                  style: TextStyle(
                                    fontFamily: "PoppinsMedium",
                                    fontSize: 12.0,
                                    color: whiteColor,
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 19.0),
                child: Container(
                  margin: EdgeInsets.only(top: 10.0),
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(10),
                    color: Color(0x0ffFFF2D1),
                    border: Border.all(
                      color: Color(0x0ffFBBC1B),
                    ),
                  ),
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Padding(
                        padding: const EdgeInsets.all(16.0),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            Row(
                              children: [
                                Image.asset("images/icons/attention.png"),
                                SizedBox(
                                  width: 10,
                                ),
                                Text(
                                  "Perhatian",
                                  style: TextStyle(
                                    fontFamily: "PoppinsRegular",
                                    fontSize: 12.0,
                                    color: Color(0x0ff535353),
                                  ),
                                ),
                              ],
                            ),
                            Container(
                              width: 316,
                              child: Text(
                                "*Termasuk penyaluran dana untuk penerima donasi dan biaya admin oleh mitra \n**5% dari donasi terkumpul",
                                style: TextStyle(
                                  fontFamily: "PoppinsRegular",
                                  fontSize: 12.0,
                                  color: Color(0x0ff535353),
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }
}
