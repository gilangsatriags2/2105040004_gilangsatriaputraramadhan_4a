import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:login_app_hci/views/home.dart';
import 'package:login_app_hci/views/theme.dart';
import 'package:login_app_hci/widgets/custom_appbar.dart';
import '../widgets/dropdown.dart';

class StatistikPage extends StatefulWidget {
  const StatistikPage({super.key});

  @override
  State<StatistikPage> createState() => _StatistikPageState();
}

class _StatistikPageState extends State<StatistikPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: CustomAppBar(
        title: "Statistik",
        onBackButtonPressed: () {
          Navigator.push(
            context,
            MaterialPageRoute(
              builder: (context) => HomePage(),
            ),
          );
        },
      ),
      body: ListView(
        children: <Widget>[
          Column(
            children: [
              Padding(
                padding: const EdgeInsets.only(top: 17.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Container(
                      width: 219.0,
                      height: 39.0,
                      decoration: BoxDecoration(
                        border: Border.all(
                          color: greenColor,
                        ),
                        borderRadius: BorderRadius.circular(25.0),
                      ),
                      child: TextFormField(
                        textAlignVertical: TextAlignVertical.center,
                        decoration: const InputDecoration(
                          border: OutlineInputBorder(
                            borderRadius: BorderRadius.all(Radius.circular(25)),
                            borderSide: BorderSide(
                              width: 0,
                              style: BorderStyle.none,
                            ),
                          ),
                          filled: true,
                          fillColor: greyColor,
                          hintText: '"Bandung"',
                          hintStyle: TextStyle(
                            fontSize: 13,
                            fontFamily: "PoppinsRegular",
                            color: Color(0xff7d7d7d),
                          ),
                          prefixIcon: Icon(
                            Icons.search,
                            color: greenColor,
                          ),
                          prefixIconConstraints: BoxConstraints(
                            minWidth: 40,
                            minHeight: 40,
                          ),
                          contentPadding: EdgeInsets.symmetric(horizontal: 10),
                        ),
                      ),
                    ),
                    SizedBox(
                      width: 10.0,
                    ),
                    Dropdown(),
                  ],
                ),
              ),
              Padding(
                padding: const EdgeInsets.symmetric(
                    horizontal: 30.0, vertical: 16.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Container(
                      width: 179.0,
                      child: Text(
                        "Penyandang Masalah Kesejahteraan Sosial",
                        style: TextStyle(
                          fontFamily: "PoppinsMedium",
                          fontSize: 15.0,
                          color: blackColor,
                        ),
                      ),
                    ),
                    Row(
                      children: <Widget>[
                        Icon(
                          Icons.location_on_outlined,
                          color: blackColor,
                        ),
                        Text(
                          "Bandung",
                          style: TextStyle(
                            fontFamily: "PoppinsMedium",
                            fontSize: 14.0,
                            color: blackColor,
                          ),
                        )
                      ],
                    ),
                  ],
                ),
              ),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 24.0),
                child: Column(
                  children: [
                    Image.asset("images/grafik.png"),
                    SizedBox(
                      height: 17.0,
                    ),
                    Container(
                      margin: EdgeInsets.only(bottom: 12.0),
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(10),
                        color: Color(0x0ffFFF2D1),
                        border: Border.all(
                          color: Color(0x0ffFBBC1B),
                        ),
                      ),
                      child: Container(
                        child: Padding(
                          padding: const EdgeInsets.all(9.0),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              Row(
                                children: [
                                  Image.asset("images/icons/info_yeloow.png"),
                                  SizedBox(
                                    width: 10,
                                  ),
                                  Text(
                                    "Perhatian",
                                    style: TextStyle(
                                      fontFamily: "PoppinsRegular",
                                      fontSize: 12.0,
                                      color: Color(0x0ff535353),
                                    ),
                                  ),
                                ],
                              ),
                              SizedBox(
                                height: 5.0,
                              ),
                              Padding(
                                padding: const EdgeInsets.symmetric(
                                    horizontal: 15.0),
                                child: Text(
                                  "*DB    : Disabilitas \n*LS     : Lansia terlantar \n*KBA  : Korban Bencana Alam \n*FM    : Fakir Miskin \n*AJ     : Anak Jalanan \n*DLL   : Dan Lain Lain ",
                                  style: TextStyle(
                                    fontFamily: "PoppinsRegular",
                                    fontSize: 12.0,
                                    color: Color(0x0ff535353),
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              SizedBox(
                height: 20.0,
              ),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 41.0),
                child: Container(
                  decoration: BoxDecoration(
                    color: greenColor,
                    borderRadius: BorderRadius.circular(10.0),
                    boxShadow: [
                      BoxShadow(
                        blurRadius: 4,
                        offset: Offset(4, 4),
                        color: Color.fromRGBO(0, 0, 0, 0.25),
                        spreadRadius: 0,
                      ),
                    ],
                  ),
                  child: Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 27.0),
                    child: TextButton(
                      onPressed: () {},
                      child: Text(
                        "Buat campaign donasi berdasarkan rekomendasi statistik",
                        style: TextStyle(
                          fontFamily: "PoppinsMedium",
                          fontSize: 14.0,
                          color: whiteColor,
                        ),
                      ),
                    ),
                  ),
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }
}
