import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:login_app_hci/views/profile.dart';
import 'package:login_app_hci/widgets/bottom_navbar.dart';

import '../service/DocIdProvider.dart';
import 'register.dart';
import 'theme.dart';

class LoginPage extends StatelessWidget {
  LoginPage({super.key});

  final TextEditingController emailController = TextEditingController();
  final docIdProvider = DocIdProvider();
  final TextEditingController passwordController = TextEditingController();

  Future signIn() async {
    try {
      await FirebaseAuth.instance.signInWithEmailAndPassword(
        email: emailController.text,
        password: passwordController.text,
      );
      // User? currentUser = FirebaseAuth.instance.currentUser;
      // String loggedInEmail = emailController.text;

      // QuerySnapshot querySnapshot = await FirebaseFirestore.instance
      //     .collection('users')
      //     .where('email', isEqualTo: loggedInEmail)
      //     .get();

      // if (querySnapshot.size > 0) {
      //   String docId = querySnapshot.docs[0].id;
      //   print('docId: $docId');
      // } else {
      //   print('No document found for the logged-in email.');
      // }
      QuerySnapshot querySnapshot = await FirebaseFirestore.instance
          .collection('users')
          .where('email', isEqualTo: emailController.text)
          .limit(1)
          .get();
      if (querySnapshot.size > 0) {
        String docId = querySnapshot.docs[0].id;
        print(docId);
        // ProfilePage(docId: docId);
        // BottomNavbar(
        //   docId: docId,
        // );
        await docIdProvider.fetchDocId(emailController.text);
      } else {
        print('No document found for the logged-in email.');
      }
    } on FirebaseAuthException catch (e) {
      if (e.code == 'user-not-found') {
        print('No user found for that email.');
      } else if (e.code == 'wrong-password') {
        print('Wrong password provided for that user.');
      }
    }
  }

  Future<UserCredential> signInWithGoogle() async {
    // Trigger the authentication flow
    final GoogleSignInAccount? googleUser = await GoogleSignIn().signIn();

    // Obtain the auth details from the request
    final GoogleSignInAuthentication? googleAuth =
        await googleUser?.authentication;

    // Create a new credential
    final credential = GoogleAuthProvider.credential(
      accessToken: googleAuth?.accessToken,
      idToken: googleAuth?.idToken,
    );

    // Once signed in, return the UserCredential
    return await FirebaseAuth.instance.signInWithCredential(credential);
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        backgroundColor: greenColor,
        body: ListView(
          children: [
            Container(
              child: Column(
                children: [
                  Container(
                    height: 192,
                    alignment: Alignment.center,
                    child: Column(
                      children: [
                        SizedBox(
                          height: 87,
                        ),
                        SvgPicture.asset('images/Charity.svg')
                      ],
                    ),
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.only(
                        bottomLeft: Radius.circular(100),
                      ),
                      color: whiteColor,
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.only(
                      top: 5,
                    ),
                    alignment: Alignment.center,
                    child: Column(
                      children: [
                        Image.asset(
                          'images/Welcome.png',
                        ),
                      ],
                    ),
                  ),
                  SizedBox(
                    height: 0.46,
                  ),
                  Container(
                    padding: EdgeInsets.only(
                      left: 26,
                      right: 26,
                      top: 30,
                    ),
                    width: MediaQuery.of(context).size.width,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.only(
                        topLeft: Radius.circular(30),
                        topRight: Radius.circular(30),
                      ),
                      color: secondaryWhiteColor,
                    ),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          'Email',
                          style: TextStyle(
                            fontSize: 14,
                            fontFamily: 'PoppinsRegular',
                          ),
                        ),
                        SizedBox(
                          height: 3,
                        ),
                        SizedBox(
                          width: double.infinity,
                          height: 48,
                          child: TextFormField(
                            controller: emailController,
                            textAlignVertical: TextAlignVertical.center,
                            decoration: InputDecoration(
                              border: OutlineInputBorder(
                                borderRadius:
                                    BorderRadius.all(Radius.circular(10)),
                                borderSide: BorderSide(
                                  width: 0,
                                  style: BorderStyle.none,
                                ),
                              ),
                              filled: true,
                              fillColor: greyColor,
                              hintText: 'Email',
                              hintStyle: TextStyle(
                                fontSize: 13,
                                color: blackColor,
                              ),
                              prefixIcon: Padding(
                                padding: EdgeInsets.only(
                                  top: 10,
                                  bottom: 9.76,
                                  right: 18,
                                  left: 28,
                                ),
                                child: Image.asset(
                                  'images/User.png',
                                ),
                              ),
                            ),
                          ),
                        ),
                        SizedBox(
                          height: 23,
                        ),
                        Text(
                          'Password',
                          style: TextStyle(
                            fontSize: 14,
                            fontFamily: 'PoppinsRegular',
                          ),
                        ),
                        SizedBox(
                          height: 3,
                        ),
                        SizedBox(
                          width: double.infinity,
                          height: 48,
                          child: TextFormField(
                            controller: passwordController,
                            obscureText: true,
                            textAlignVertical: TextAlignVertical.center,
                            decoration: InputDecoration(
                              border: OutlineInputBorder(
                                borderRadius:
                                    BorderRadius.all(Radius.circular(10)),
                                borderSide: BorderSide(
                                  width: 0,
                                  style: BorderStyle.none,
                                ),
                              ),
                              filled: true,
                              fillColor: greyColor,
                              hintText: 'Password',
                              hintStyle: TextStyle(
                                fontSize: 13,
                                color: blackColor,
                              ),
                              prefixIcon: Padding(
                                padding: EdgeInsets.only(
                                  top: 10,
                                  bottom: 9.76,
                                  right: 21,
                                  left: 32,
                                ),
                                child: Image.asset(
                                  'images/Key.png',
                                ),
                              ),
                            ),
                          ),
                        ),
                        SizedBox(
                          height: 18,
                        ),
                        SizedBox(
                          width: double.infinity,
                          child: Text(
                            'Kamu lupa password ? ',
                            style: TextStyle(
                              color: greenColor,
                            ),
                            textAlign: TextAlign.end,
                          ),
                        ),
                        SizedBox(
                          height: 18.0,
                        ),
                        SizedBox(
                          width: double.infinity,
                          height: 48,
                          child: ElevatedButton(
                            style: ElevatedButton.styleFrom(
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(10.0),
                              ),
                              backgroundColor: greenColor,
                            ),
                            onPressed: () {
                              signIn();
                            },
                            child: Text(
                              "Login",
                              style: TextStyle(
                                color: whiteColor,
                                fontFamily: "PoppinsMedium",
                                fontSize: 14,
                              ),
                            ),
                          ),
                        ),
                        SizedBox(
                          height: 10.0,
                        ),
                        Row(
                          children: <Widget>[
                            Expanded(
                              child: Divider(
                                color: blackColor,
                                thickness: 1,
                                indent: 3.0,
                                endIndent: 13.0,
                              ),
                            ),
                            Text(
                              "or",
                              style: TextStyle(
                                fontFamily: "PoppinsRegular",
                                fontSize: 14,
                              ),
                            ),
                            Expanded(
                              child: Divider(
                                color: blackColor,
                                thickness: 1,
                                indent: 13.0,
                                endIndent: 3.0,
                              ),
                            ),
                          ],
                        ),
                        SizedBox(
                          height: 10,
                        ),
                        SizedBox(
                          width: double.infinity,
                          height: 48.0,
                          child: ElevatedButton.icon(
                            icon: Image.asset('images/Google.png'),
                            label: Text(
                              "Login Google",
                              style: TextStyle(
                                color: blackColor,
                                fontFamily: "PoppinsMedium",
                                fontSize: 14,
                              ),
                            ),
                            onPressed: () {
                              signInWithGoogle();
                            },
                            style: ElevatedButton.styleFrom(
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(10.0),
                              ),
                              backgroundColor: greyColor,
                            ),
                          ),
                        ),
                        SizedBox(
                          height: 15.0,
                        ),
                        SizedBox(
                          width: double.infinity,
                          child: InkWell(
                            child: RichText(
                              textAlign: TextAlign.center,
                              text: TextSpan(
                                text: 'Kamu belum punya akun ? ',
                                style: TextStyle(
                                  fontFamily: "PoppinsRegular",
                                  fontSize: 14,
                                  color: blackColor,
                                ),
                                children: <InlineSpan>[
                                  TextSpan(
                                    text: 'Daftar disini',
                                    style: TextStyle(
                                      fontFamily: "PoppinsRegular",
                                      fontSize: 14,
                                      color: greenColor,
                                    ),
                                    recognizer: TapGestureRecognizer()
                                      ..onTap = () {
                                        Navigator.push(
                                          context,
                                          MaterialPageRoute(
                                            builder: (context) =>
                                                RegisterPage(),
                                          ),
                                        );
                                      },
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ),
                        SizedBox(
                          height: 20,
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
