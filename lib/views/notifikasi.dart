import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:login_app_hci/views/home.dart';
import 'package:login_app_hci/views/theme.dart';
import 'package:login_app_hci/widgets/berita_card.dart';
import 'package:login_app_hci/widgets/custom_appbar.dart';

class NotifikasiPage extends StatefulWidget {
  const NotifikasiPage({super.key});

  @override
  State<NotifikasiPage> createState() => _NotifikasiPageState();
}

class _NotifikasiPageState extends State<NotifikasiPage> {
  String activeTab = "Berita";

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: whiteColor,
      appBar: CustomAppBar(
          title: "Notifikasi",
          onBackButtonPressed: () {
            Navigator.push(
                context, MaterialPageRoute(builder: (context) => HomePage()));
          }),
      body: ListView(children: [
        Column(
          children: [
            SizedBox(
              height: 16.0,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                Column(
                  children: [
                    InkWell(
                      onTap: () {
                        setState(() {
                          activeTab = "Berita";
                        });
                      },
                      child: Text(
                        "Berita",
                        style: TextStyle(
                          fontFamily: "PoppinsRegular",
                          fontSize: 15.0,
                          color: blackColor,
                        ),
                      ),
                    ),
                    if (activeTab == "Berita")
                      Container(
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(100),
                        ),
                        width: 50.0,
                        child: const Divider(
                          thickness: 4.0,
                          color: greenColor,
                        ),
                      ),
                    if (activeTab != "Berita")
                      Container(
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(100),
                        ),
                        width: 67.0,
                        child: const Divider(
                          thickness: 4.0,
                          color: whiteColor,
                        ),
                      ),
                  ],
                ),
                Column(
                  children: [
                    InkWell(
                      onTap: () {
                        setState(() {
                          activeTab = "Pemberitahuan";
                        });
                      },
                      child: Text(
                        "Pemberitahuan",
                        style: TextStyle(
                          fontFamily: "PoppinsRegular",
                          fontSize: 15.0,
                          color: blackColor,
                        ),
                      ),
                    ),
                    if (activeTab == "Pemberitahuan")
                      Container(
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(100),
                        ),
                        width: 125.0,
                        child: const Divider(
                          thickness: 4.0,
                          color: greenColor,
                        ),
                      ),
                    if (activeTab != "Pemberitahuan")
                      Container(
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(100),
                        ),
                        width: 67.0,
                        child: const Divider(
                          thickness: 4.0,
                          color: whiteColor,
                        ),
                      ),
                  ],
                ),
              ],
            ),
            if (activeTab == "Berita")
              Visibility(
                visible: activeTab == "Berita",
                child: Column(
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 15.0),
                      child: Column(
                        children: [
                          BeritaCard(
                            text:
                                "“Aplikasi Charity v.1.0.3 sudah tersedia di Play Store dan App Store, segera perbarui aplikasi Charity”",
                          ),
                          BeritaCard(
                            text:
                                "“Jangan lupa donasi untuk membantu sesama ya :)”",
                          ),
                          BeritaCard(
                            text:
                                "“Di bulan ramadhan ini Charity membuka donasi untuk membangun masjid yey”",
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            if (activeTab == "Pemberitahuan")
              Visibility(
                visible: activeTab == "Pemberitahuan",
                child: Column(
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 15.0),
                      child: Column(
                        children: [
                          Padding(
                            padding:
                                const EdgeInsets.symmetric(horizontal: 10.0),
                            child: Container(
                              margin: EdgeInsets.only(top: 10.0),
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(10),
                                color: Color(0x0ffFFF2D1),
                                border: Border.all(
                                  color: Color(0x0ffFBBC1B),
                                ),
                              ),
                              child: Row(
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: [
                                  Padding(
                                    padding: const EdgeInsets.all(16.0),
                                    child: Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      children: <Widget>[
                                        Row(
                                          children: [
                                            Image.asset(
                                                "images/icons/attention.png"),
                                            SizedBox(
                                              width: 10,
                                            ),
                                            Text(
                                              "Perhatian",
                                              style: TextStyle(
                                                fontFamily: "PoppinsRegular",
                                                fontSize: 12.0,
                                                color: Color(0x0ff535353),
                                              ),
                                            ),
                                          ],
                                        ),
                                        SizedBox(
                                          height: 10.0,
                                        ),
                                        Container(
                                          width: 316,
                                          child: Text(
                                            "Anda sedang tidak melakukan campaing donasi, sehingga tidak ada pemberitahuan donasi masuk....",
                                            style: TextStyle(
                                              fontFamily: "PoppinsRegular",
                                              fontSize: 12.0,
                                              color: Color(0x0ff535353),
                                            ),
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
          ],
        ),
      ]),
    );
  }
}
