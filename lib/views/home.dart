import 'package:banner_carousel/banner_carousel.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:login_app_hci/views/filter.dart';
import 'package:login_app_hci/views/kategori.dart';
import 'package:login_app_hci/views/lihat_lainnya.dart';
import 'package:login_app_hci/views/more_kategori.dart';
import 'package:login_app_hci/widgets/card_donasi.dart';
import 'package:login_app_hci/widgets/list_campaign.dart';
import '../widgets/kategori.dart';
import 'theme.dart';

class HomePage extends StatefulWidget {
  const HomePage({super.key});

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  FirebaseAuth auth = FirebaseAuth.instance;
  User? user;
  List<BannerModel> listBanners = [
    BannerModel(imagePath: 'images/banner.png', id: "1"),
    BannerModel(imagePath: 'images/banner.png', id: "2"),
    BannerModel(imagePath: 'images/banner.png', id: "3"),
  ];

  List<CardDonasi> cardDonasi = [
    CardDonasi(),
    CardDonasi(),
    CardDonasi(),
  ];
  Future<String> getCurrentUser() async {
    user = await auth.currentUser;

    if (user != null) {
      String uid = user!.uid;
      return uid; // Mengembalikan UID sebagai string
    }

    throw Exception(
        "User not logged in"); // Jika user adalah null, lemparkan pengecualian atau berikan tanggapan yang sesuai
  }

  Future<String?> getDocId() async {
    try {
      String uid = await getCurrentUser();
      DocumentSnapshot documentSnapshot =
          await FirebaseFirestore.instance.collection('users').doc(uid).get();

      final data = documentSnapshot.data() as Map<String, dynamic>;
      if (data != null && data.containsKey('docId')) {
        return data['docId'];
      } else {
        return "null"; // Atau nilai default yang sesuai
      }
    } catch (error) {
      print("Error: $error");
      return null; // Atau nilai default yang sesuai jika terjadi kesalahan
    }
  }

  void logOut() async {
    await FirebaseAuth.instance.signOut();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: whiteColor,
      appBar: AppBar(
        title: Center(
          child: Container(
            child: SvgPicture.asset('images/Charity.svg'),
          ),
        ),
        backgroundColor: whiteColor,
        elevation: 0,
      ),
      body: ListView(
        children: [
          Row(
            children: [
              Container(
                margin: const EdgeInsets.only(top: 20.0, left: 20.0),
                width: 311.0,
                height: 39.0,
                child: TextFormField(
                  textAlignVertical: TextAlignVertical.center,
                  decoration: const InputDecoration(
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.all(Radius.circular(25)),
                      borderSide: BorderSide(
                        width: 0,
                        style: BorderStyle.none,
                      ),
                    ),
                    filled: true,
                    fillColor: greyColor,
                    hintText: 'coba cari "Bencana Alam"',
                    hintStyle: TextStyle(
                      fontSize: 13,
                      fontFamily: "PoppinsRegular",
                      color: Color(0xff7d7d7d),
                    ),
                    prefixIcon: Icon(Icons.search),
                    prefixIconConstraints: BoxConstraints(
                      minWidth: 40,
                      minHeight: 40,
                    ),
                    contentPadding: EdgeInsets.symmetric(horizontal: 10),
                  ),
                ),
              ),
              Container(
                padding: EdgeInsets.only(
                  top: 20.0,
                  left: 6.0,
                ),
                child: TextButton(
                  child: SvgPicture.asset("images/icons/filter.svg"),
                  onPressed: () {
                    Navigator.push(context,
                        MaterialPageRoute(builder: (context) => FilterPage()));
                  },
                ),
              ),
            ],
          ),

          SizedBox(
            height: 20,
          ),
          BannerCarousel(
            banners: listBanners,
            customizedIndicators: IndicatorModel.animation(
                width: 10, height: 10, spaceBetween: 13, widthAnimation: 30),
            height: 198,
            activeColor: greenColor,
            disableColor: Colors.white,
            animation: true,
            borderRadius: 8,
            width: 381,
            indicatorBottom: false,
          ),
          SizedBox(
            height: 20,
          ),
          Divider(
            height: 5,
            thickness: 5,
          ),
          Padding(
            padding: const EdgeInsets.only(left: 15.0, right: 4),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Text(
                  "Rekomendasi Campaign Donasi",
                  style: TextStyle(
                    fontSize: 16,
                    fontFamily: "PoppinsMedium",
                    color: blackColor,
                  ),
                ),
                TextButton(
                  onPressed: () {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => LihatLainnyaPage()));
                  },
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      Text(
                        "Lihat lainnya",
                        style: TextStyle(
                          fontFamily: "PoppinsMedium",
                          fontSize: 10.0,
                          color: blackColor,
                        ),
                      ),
                      Icon(
                        Icons.keyboard_arrow_right_rounded,
                        color: blackColor,
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
          Container(
            height: 240,
            child: ListView.builder(
              scrollDirection: Axis.horizontal,
              itemCount: cardDonasi.length,
              itemBuilder: (BuildContext context, int index) {
                return Padding(
                  padding: const EdgeInsets.only(right: 8.0, left: 15.0),
                  child: Row(
                    children: [
                      cardDonasi[index],
                    ],
                  ),
                );
              },
            ),
          ),
          SizedBox(
            height: 10.0,
          ),
          Divider(
            thickness: 5,
          ),
          Padding(
            padding: const EdgeInsets.only(left: 18.0),
            child: Row(
              children: [
                Text(
                  "Kategori",
                  style: TextStyle(
                    fontFamily: "PoppinsMedium",
                    fontSize: 16.0,
                    color: blackColor,
                  ),
                ),
              ],
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(top: 9, left: 13.0, right: 14.0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                Kategori(
                  onpressed: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (context) => KategoriPage(
                          kategori: "Bencana Alam",
                        ),
                      ),
                    );
                  },
                  height: 35.0,
                  backgroundColor: Colors.transparent,
                  borderColor: greenColor,
                  logo: "mountain",
                  textColor: null,
                  kategori: "Bencana Alam",
                ),
                SizedBox(
                  width: 5.0,
                ),
                Kategori(
                  onpressed: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (context) => KategoriPage(
                          kategori: "Kesehatan",
                        ),
                      ),
                    );
                  },
                  height: 35.0,
                  backgroundColor: Colors.transparent,
                  borderColor: greenColor,
                  logo: "kesehatan",
                  textColor: null,
                  kategori: "Kesehatan",
                ),
                SizedBox(
                  width: 5.0,
                ),
                Kategori(
                  onpressed: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (context) => KategoriPage(
                          kategori: "Sosial",
                        ),
                      ),
                    );
                  },
                  height: 35.0,
                  backgroundColor: Colors.transparent,
                  borderColor: greenColor,
                  logo: "sosial",
                  textColor: null,
                  kategori: "Sosial",
                ),
                SizedBox(
                  width: 5.0,
                ),
                Kategori(
                  onpressed: () {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => MoreKategoriPage()));
                  },
                  height: 35.0,
                  backgroundColor: Colors.transparent,
                  borderColor: greenColor,
                  logo: "more",
                  textColor: null,
                  kategori: null,
                ),
              ],
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(left: 18, top: 12, right: 10),
            child: Column(
              children: [
                ListCampaign(
                  image: "campaign",
                  widthPicture: 145.0,
                  heightPicture: 82.0,
                  campaigner: "Yayasan Peduli Anak",
                  campaign:
                      "Banyak anak kecil terlantar di Bandung, Ayo Bantu !!!!!!!!!",
                ),
                ListCampaign(
                  image: "campaign",
                  widthPicture: 145.0,
                  campaigner: "Yayasan Peduli Anak",
                  heightPicture: 82.0,
                  campaign:
                      "Banyak anak kecil terlantar di Bandung, Ayo Bantu !!!!!!!!!",
                ),
                ListCampaign(
                  image: "campaign",
                  widthPicture: 145.0,
                  campaigner: "Yayasan Peduli Anak",
                  campaign:
                      "Banyak anak kecil terlantar di Bandung, Ayo Bantu !!!!!!!!!",
                  heightPicture: 82.0,
                ),
                SizedBox(
                  height: 15.0,
                ),
              ],
            ),
          )
          // TextButton(onPressed: logOut, child: Text("Logout")),
        ],
      ),
    );
  }
}
