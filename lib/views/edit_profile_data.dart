import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/cupertino.dart';
import 'package:loading_animation_widget/loading_animation_widget.dart';

import 'form_edit_profile.dart';

class EditProfileData extends StatefulWidget {
  final String docId;
  EditProfileData({required this.docId});

  @override
  _EditProfileDataState createState() => _EditProfileDataState();
}

class _EditProfileDataState extends State<EditProfileData> {
  FirebaseFirestore firestore = FirebaseFirestore.instance;
  CollectionReference users = FirebaseFirestore.instance.collection('users');
  Map<String, dynamic>? data;
  double _kSize = 100;

  @override
  void initState() {
    super.initState();
    fetchData();
  }

  Future<void> fetchData() async {
    final snapshot = await users.doc(widget.docId).get();
    if (snapshot.exists) {
      setState(() {
        data = snapshot.data() as Map<String, dynamic>;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    if (data == null) {
      return Container(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Center(
              child: LoadingAnimationWidget.waveDots(
                color: Color.fromRGBO(158, 158, 158, 0.25),
                size: _kSize,
              ),
            ),
          ],
        ),
      );
    } else {
      return EditProfileForm(
        data: data!,
        docId: widget.docId,
      );
    }
  }
}
