import 'package:expandable_text/expandable_text.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_svg/svg.dart';
import 'package:login_app_hci/views/detail_pembuat_campaign.dart';
import 'package:login_app_hci/views/rincian_dana.dart';
import 'package:login_app_hci/views/theme.dart';
import 'package:login_app_hci/widgets/kategori.dart';
import 'package:login_app_hci/widgets/komentar_donasi.dart';
import 'package:login_app_hci/widgets/pendonasi.dart';
import 'package:login_app_hci/widgets/tombol_donasi.dart';
import 'package:step_progress_indicator/step_progress_indicator.dart';

class DetailCampaign extends StatefulWidget {
  const DetailCampaign({super.key});

  @override
  State<DetailCampaign> createState() => _DetailCampaignState();
}

class _DetailCampaignState extends State<DetailCampaign> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: whiteColor,
      appBar: AppBar(
        title: Container(
          margin: EdgeInsets.only(left: 50),
          child: SvgPicture.asset('images/Charity.svg'),
        ),
        leading: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Container(
            width: 38.0,
            height: 38.0,
            decoration: BoxDecoration(
                color: greenColor, borderRadius: BorderRadius.circular(100)),
            child: IconButton(
              onPressed: () {
                Navigator.pop(context);
              },
              icon: Icon(Icons.arrow_back_ios_new_rounded),
              color: blackColor,
            ),
          ),
        ),
        backgroundColor: whiteColor,
        elevation: 0,
      ),
      body: Stack(
        children: [
          ListView(
            children: [
              Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Padding(
                    padding: const EdgeInsets.only(
                        top: 18.0, left: 19.0, right: 19.0),
                    child: Column(
                      children: [
                        Image.asset("images/paul_large.png"),
                        SizedBox(
                          height: 14.0,
                        ),
                        ExpandableText(
                          "Bantu kakek paul untuk mendapatkan tempat tinggal",
                          expandText: 'more',
                          linkColor: greenColor,
                          collapseText: 'less',
                          maxLines: 2,
                          style: TextStyle(
                            fontFamily: "PoppinsMedium",
                            fontSize: 19.0,
                            color: blackColor,
                          ),
                        ),
                        SizedBox(
                          height: 1,
                        ),
                        Row(
                          children: [
                            Image.asset("images/ypl.png"),
                            TextButton(
                              onPressed: () {
                                Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                    builder: (context) =>
                                        DetailPembuatCampaignPage(),
                                  ),
                                );
                              },
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.start,
                                children: [
                                  Text(
                                    "Yayasan Peduli Lansia",
                                    style: TextStyle(
                                      fontFamily: "PoppinsRegular",
                                      fontSize: 15,
                                      color: blackColor,
                                    ),
                                  ),
                                  SizedBox(
                                    width: 5,
                                  ),
                                  Image.asset("images/icons/check.png"),
                                ],
                              ),
                            ),
                          ],
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Row(
                              children: [
                                Kategori(
                                  onpressed: () {},
                                  height: 35.0,
                                  logo: "sosial",
                                  kategori: "Sosial",
                                  backgroundColor: Colors.transparent,
                                  textColor: null,
                                  borderColor: greenColor,
                                ),
                                SizedBox(
                                  width: 9.0,
                                ),
                                Kategori(
                                  onpressed: () {},
                                  height: 35.0,
                                  logo: "ekonomi",
                                  kategori: "Ekonomi",
                                  backgroundColor: Colors.transparent,
                                  borderColor: greenColor,
                                  textColor: null,
                                ),
                              ],
                            ),
                            Kategori(
                              onpressed: () {},
                              height: 35.0,
                              logo: "share",
                              kategori: "Bagikan",
                              backgroundColor: greenColor,
                              borderColor: Colors.transparent,
                              textColor: whiteColor,
                            )
                          ],
                        ),
                        SizedBox(
                          height: 13,
                        ),
                        StepProgressIndicator(
                          totalSteps: 100,
                          currentStep: 50,
                          size: 5,
                          padding: 0,
                          selectedColor: Colors.yellow,
                          unselectedColor: Colors.cyan,
                          roundedEdges: Radius.circular(10),
                          selectedGradientColor: LinearGradient(
                            begin: Alignment.topLeft,
                            end: Alignment.bottomRight,
                            colors: [
                              Color.fromRGBO(86, 191, 191, 100),
                              Color.fromRGBO(86, 191, 191, 100)
                            ],
                          ),
                          unselectedGradientColor: LinearGradient(
                            begin: Alignment.topLeft,
                            end: Alignment.bottomRight,
                            colors: [
                              Color.fromRGBO(204, 204, 204, 100),
                              Color.fromRGBO(204, 204, 204, 100),
                            ],
                          ),
                        ),
                        SizedBox(
                          height: 8.0,
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Text(
                              "Terkumpul Rp. 5.000.000,00 (50%)",
                              style: TextStyle(
                                  fontFamily: "PoppinsSB",
                                  fontSize: 13,
                                  color: greenColor),
                            ),
                            Text(
                              "3 Hari lagi",
                              style: TextStyle(
                                fontFamily: "PoppinsMedium",
                                fontSize: 13,
                                color: blackColor,
                              ),
                            ),
                          ],
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Text(
                              "Target Rp. 10.000.000,00",
                              style: TextStyle(
                                  fontFamily: "PoppinsSB",
                                  fontSize: 13,
                                  color: Color(0x0ffa4a4a4)),
                            ),
                            Text(
                              "100 Donasi",
                              style: TextStyle(
                                fontFamily: "PoppinsMedium",
                                fontSize: 13,
                                color: blackColor,
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ),
                  Divider(
                    height: 25,
                    thickness: 5,
                  ),
                  Padding(
                    padding: EdgeInsets.only(left: 19.0, right: 19.0),
                    child: Column(
                      children: [
                        Container(
                          width: 390.0,
                          height: 51.0,
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(20),
                            color: Color(0x0ffe8e8e8),
                          ),
                          child: Column(
                            children: [
                              Padding(
                                padding: const EdgeInsets.only(
                                    left: 13.0, right: 13.0),
                                child: TextButton(
                                  onPressed: () {
                                    Navigator.push(
                                        context,
                                        MaterialPageRoute(
                                            builder: (context) =>
                                                RincianDanaPage()));
                                  },
                                  child: Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    children: [
                                      Row(
                                        children: [
                                          Image.asset(
                                              "images/icons/wallet.png"),
                                          SizedBox(
                                            width: 14.0,
                                          ),
                                          Text(
                                            "Rincian Donasi",
                                            style: TextStyle(
                                              fontFamily: "PoppinsMedium",
                                              fontSize: 14.0,
                                              color: blackColor,
                                            ),
                                          ),
                                        ],
                                      ),
                                      Image.asset(
                                          "images/icons/arrow_more.png"),
                                    ],
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                        Divider(
                          height: 25.0,
                          thickness: 1,
                          color: greenColor,
                        ),
                        Container(
                          width: 390.0,
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(20),
                            color: Color(0x0ffe8e8e8),
                          ),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Padding(
                                padding: const EdgeInsets.only(
                                    top: 14.0, left: 12.0),
                                child: Text(
                                  "Tentang Campaign Donasi",
                                  style: TextStyle(
                                    fontFamily: "PoppinsSB",
                                    fontSize: 13.0,
                                  ),
                                ),
                              ),
                              Padding(
                                padding: const EdgeInsets.only(
                                    top: 2.0,
                                    left: 27.0,
                                    right: 11.0,
                                    bottom: 14.0),
                                child: ExpandableText(
                                  "Donasi ini nantinya akan disalurkan kepada kakek paul yang tidak mempunyai rumah. Tim dari Yayasan Peduli Lansia akan memberikan dan membangun fasilitas tempat tinggal dan akan ditinggali oleh kakek paul. Tidak hanya itu semua biaya akan ditanggung oleh Yayasan Peduli Lansia melalui donasi ini",
                                  expandText: 'lanjutkan membaca',
                                  linkColor: greenColor,
                                  collapseText: 'lebih sedikit',
                                  maxLines: 5,
                                  style: TextStyle(
                                    fontFamily: "PoppinsRegular",
                                    fontSize: 13.0,
                                    color: blackColor,
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                        Divider(
                          height: 25.0,
                          thickness: 1,
                          color: greenColor,
                        ),
                        Container(
                          width: 390.0,
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(20),
                            color: Color(0x0ffededed),
                          ),
                          child: Padding(
                            padding: const EdgeInsets.symmetric(
                                horizontal: 20.0, vertical: 20.0),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              // mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Align(
                                  alignment: Alignment.topLeft,
                                  child: Text(
                                    "Pendonasi (100)",
                                    style: TextStyle(
                                      fontFamily: "PoppinsSB",
                                      fontSize: 13.0,
                                    ),
                                  ),
                                ),
                                SizedBox(
                                  height: 6,
                                ),
                                Pendonasi(),
                                Pendonasi(),
                                Container(
                                  height: 35.0,
                                  width: 150.0,
                                  decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(10),
                                    color: greenColor,
                                  ),
                                  child: Padding(
                                    padding: const EdgeInsets.symmetric(
                                      vertical: 2.0,
                                      horizontal: 6.0,
                                    ),
                                    child: TextButton(
                                      onPressed: () {},
                                      child: Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceBetween,
                                        children: [
                                          Text(
                                            "Lihat selengkapnya",
                                            style: TextStyle(
                                              fontFamily: "PoppinsRegular",
                                              fontSize: 10,
                                              color: whiteColor,
                                            ),
                                          ),
                                          SizedBox(
                                            width: 5.0,
                                          ),
                                          Image.asset(
                                            "images/icons/arrow_bottom.png",
                                          ),
                                        ],
                                      ),
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                        Divider(
                          height: 25.0,
                          thickness: 1,
                          color: greenColor,
                        ),
                        Container(
                          width: 390.0,
                          decoration: BoxDecoration(
                            color: Color(0x0ffEDEDED),
                            borderRadius: BorderRadius.circular(10),
                          ),
                          child: Padding(
                            padding: const EdgeInsets.all(20.0),
                            child: Column(
                              children: [
                                Align(
                                  alignment: Alignment.topLeft,
                                  child: Text(
                                    "Komentar Orang yang Donasi",
                                    style: TextStyle(
                                      fontFamily: "PoppinsSB",
                                      fontSize: 13.0,
                                      color: blackColor,
                                    ),
                                  ),
                                ),
                                SizedBox(
                                  height: 6.0,
                                ),
                                KomentarDonasi(),
                                KomentarDonasi(),
                                Container(
                                  height: 35.0,
                                  width: 150.0,
                                  decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(10),
                                    color: greenColor,
                                  ),
                                  child: Padding(
                                    padding: const EdgeInsets.symmetric(
                                      vertical: 2.0,
                                      horizontal: 6.0,
                                    ),
                                    child: TextButton(
                                      onPressed: () {},
                                      child: Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceBetween,
                                        children: [
                                          Text(
                                            "Lihat selengkapnya",
                                            style: TextStyle(
                                              fontFamily: "PoppinsRegular",
                                              fontSize: 10,
                                              color: whiteColor,
                                            ),
                                          ),
                                          SizedBox(
                                            width: 5.0,
                                          ),
                                          Image.asset(
                                            "images/icons/arrow_bottom.png",
                                          ),
                                        ],
                                      ),
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                        Divider(
                          height: 25.0,
                          thickness: 1,
                          color: greenColor,
                        ),
                      ],
                    ),
                  )
                ],
              ),
            ],
          ),
          Positioned(
            left: (MediaQuery.of(context).size.width - 245) / 2,
            right: (MediaQuery.of(context).size.width - 245) / 2,
            top: 620,
            child: TombolDonasi(),
          ),
        ],
      ),
    );
  }
}
