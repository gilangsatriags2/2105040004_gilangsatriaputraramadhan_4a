import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:login_app_hci/views/status_pembayaran_dompet_donasi.dart';
import 'package:login_app_hci/views/theme.dart';
import 'package:login_app_hci/widgets/custom_appbar.dart';
import 'package:login_app_hci/widgets/custom_button2.dart';
import 'package:login_app_hci/widgets/radio_button.dart';
import 'package:login_app_hci/widgets/text_field.dart';

class DonasiPage extends StatefulWidget {
  const DonasiPage({super.key});

  @override
  State<DonasiPage> createState() => _DonasiPageState();
}

class _DonasiPageState extends State<DonasiPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: CustomAppBar(
        title: "Donasi",
        onBackButtonPressed: () {
          Navigator.pop(context);
        },
      ),
      body: ListView(
        children: <Widget>[
          Padding(
            padding: EdgeInsets.only(left: 22, top: 15.0, right: 18),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text(
                  "Masukkan nominal donasi",
                  style: TextStyle(
                    fontFamily: "PoppinsRegular",
                    fontSize: 15.0,
                    color: blackColor,
                  ),
                ),
                SizedBox(
                  height: 8.0,
                ),
                Row(
                  children: [
                    Column(
                      children: [
                        RadioButton(
                            nominal: "Rp. 10.000,00", title: "Rp. 10.000,00"),
                        SizedBox(
                          height: 10.0,
                        ),
                        RadioButton(
                            nominal: "Rp. 20.000,00", title: "Rp. 20.000,00"),
                      ],
                    ),
                    SizedBox(width: 10.0),
                    Column(
                      children: [
                        RadioButton(
                            nominal: "Rp. 50.000,00", title: "Rp. 50.000,00"),
                        SizedBox(
                          height: 10.0,
                        ),
                        RadioButton(
                            nominal: "Rp. 100.000,00", title: "Rp. 100.000,00"),
                      ],
                    ),
                  ],
                ),
                SizedBox(
                  height: 10,
                ),
                Text(
                  "Nominal donasi lainnya",
                  style: TextStyle(
                    fontFamily: "PoppinsRegular",
                    fontSize: 15.0,
                    color: blackColor,
                  ),
                ),
                SizedBox(
                  height: 10,
                ),
                CustomTextField(
                  width: 370.0,
                  hintText: "Minimal donasi Rp. 1.000,00",
                  color: Color(0x0ffE9E9E9),
                  height: 45.0,
                ),
                Divider(
                  height: 30.0,
                  thickness: 1,
                  color: greenColor,
                ),
                Text(
                  "Pesan untuk penerima donasi",
                  style: TextStyle(
                    fontFamily: "PoppinsRegular",
                    fontSize: 15.0,
                    color: blackColor,
                  ),
                ),
                SizedBox(
                  height: 10,
                ),
                CustomTextField(
                  width: 370.0,
                  hintText: "",
                  color: Color(0x0ffE9E9E9),
                  height: 82.0,
                ),
                Divider(
                  height: 30.0,
                  thickness: 1,
                  color: greenColor,
                ),
                CustomButton2(
                  saldo: "(Saldo Rp. 100.000,00)",
                  width: 371.0,
                  height: 54.0,
                  onpressed: () {},
                  icon: "charity",
                ),
                CustomButton2(
                  saldo: "",
                  width: 371.0,
                  height: 54.0,
                  onpressed: () {},
                  icon: "dana",
                ),
                CustomButton2(
                  saldo: "",
                  width: 371.0,
                  height: 54.0,
                  onpressed: () {},
                  icon: "gopay",
                ),
                CustomButton2(
                  saldo: "",
                  width: 371.0,
                  height: 54.0,
                  onpressed: () {},
                  icon: "ovo",
                ),
                CustomButton2(
                  saldo: "",
                  width: 371.0,
                  height: 54.0,
                  onpressed: () {},
                  icon: "shopee",
                ),
                CustomButton2(
                  saldo: "",
                  width: 371.0,
                  height: 54.0,
                  onpressed: () {},
                  icon: "bank",
                ),
                SizedBox(
                  height: 10,
                ),
              ],
            ),
          ),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 19.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Container(
                  margin: EdgeInsets.only(bottom: 12.0),
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(10),
                    color: Color(0x0ffFFF2D1),
                    border: Border.all(
                      color: Color(0x0ffFBBC1B),
                    ),
                  ),
                  child: Container(
                    child: Padding(
                      padding: const EdgeInsets.all(9.0),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          Row(
                            children: [
                              Image.asset("images/icons/info_yeloow.png"),
                              SizedBox(
                                width: 10,
                              ),
                              Text(
                                "Perhatian",
                                style: TextStyle(
                                  fontFamily: "PoppinsRegular",
                                  fontSize: 12.0,
                                  color: Color(0x0ff535353),
                                ),
                              ),
                            ],
                          ),
                          SizedBox(
                            height: 5.0,
                          ),
                          Padding(
                            padding:
                                const EdgeInsets.symmetric(horizontal: 15.0),
                            child: Text(
                              "Nominal di atas termasuk 5% untuk dana operasional Charity :)",
                              style: TextStyle(
                                fontFamily: "PoppinsRegular",
                                fontSize: 12.0,
                                color: Color(0x0ff535353),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 112),
            child: Container(
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(10),
                color: greenColor,
              ),
              child: Padding(
                padding: const EdgeInsets.symmetric(
                  vertical: 2.0,
                  horizontal: 6.0,
                ),
                child: TextButton(
                  onPressed: () {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) =>
                                StatusPembayaranDompetDonasiPage()));
                  },
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text(
                        "Lanjutkan Donasi",
                        style: TextStyle(
                          fontFamily: "PoppinsRegular",
                          fontSize: 15.0,
                          color: whiteColor,
                        ),
                      ),
                      SizedBox(
                        width: 5.0,
                      ),
                      Image.asset(
                        "images/icons/arrow_right.png",
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ),
          SizedBox(
            height: 20,
          ),
        ],
      ),
    );
  }
}
