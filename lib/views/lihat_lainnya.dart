import 'package:flutter/material.dart';
import 'package:login_app_hci/widgets/custom_appbar.dart';
import 'package:login_app_hci/widgets/list_campaign.dart';

class LihatLainnyaPage extends StatefulWidget {
  const LihatLainnyaPage({super.key});

  @override
  State<LihatLainnyaPage> createState() => _LihatLainnyaPageState();
}

class _LihatLainnyaPageState extends State<LihatLainnyaPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: CustomAppBar(
          title: "Rekomendasi Campaign Donasi",
          onBackButtonPressed: () {
            Navigator.pop(context);
          }),
      body: ListView(
        children: [
          Padding(
            padding: const EdgeInsets.symmetric(vertical: 10, horizontal: 10),
            child: Column(
              children: [
                ListCampaign(
                  image: "lansia",
                  widthPicture: 145.0,
                  heightPicture: 82.0,
                  campaign:
                      "Ayo Bantu Para Lansia Terlantara di Wilayah Magelang  ",
                  campaigner: "Yayasan Peduli Lansia",
                ),
                ListCampaign(
                  image: "paul",
                  widthPicture: 145.0,
                  heightPicture: 82.0,
                  campaign: "Bantu kakek paul untuk mendapatkan tempat tinggal",
                  campaigner: "Yayasan Peduli Lansia",
                ),
                ListCampaign(
                  image: "campaign",
                  widthPicture: 145.0,
                  heightPicture: 82.0,
                  campaign:
                      "Banyak anak kecil terlantar di Bandung, Ayo Bantu !!!!!!!!!",
                  campaigner: "Yayasan Peduli Anak",
                ),
                ListCampaign(
                  image: "lansia",
                  widthPicture: 145.0,
                  heightPicture: 82.0,
                  campaign:
                      "Ayo Bantu Para Lansia Terlantara di Wilayah Magelang  ",
                  campaigner: "Yayasan Peduli Lansia",
                ),
                ListCampaign(
                  image: "paul",
                  widthPicture: 145.0,
                  heightPicture: 82.0,
                  campaign: "Bantu kakek paul untuk mendapatkan tempat tinggal",
                  campaigner: "Yayasan Peduli Lansia",
                ),
                ListCampaign(
                  image: "campaign",
                  widthPicture: 145.0,
                  heightPicture: 82.0,
                  campaign:
                      "Banyak anak kecil terlantar di Bandung, Ayo Bantu !!!!!!!!!",
                  campaigner: "Yayasan Peduli Anak",
                ),
                ListCampaign(
                  image: "lansia",
                  widthPicture: 145.0,
                  heightPicture: 82.0,
                  campaign:
                      "Ayo Bantu Para Lansia Terlantara di Wilayah Magelang  ",
                  campaigner: "Yayasan Peduli Lansia",
                ),
                ListCampaign(
                  image: "paul",
                  widthPicture: 145.0,
                  heightPicture: 82.0,
                  campaign: "Bantu kakek paul untuk mendapatkan tempat tinggal",
                  campaigner: "Yayasan Peduli Lansia",
                ),
                ListCampaign(
                  image: "campaign",
                  widthPicture: 145.0,
                  heightPicture: 82.0,
                  campaign:
                      "Banyak anak kecil terlantar di Bandung, Ayo Bantu !!!!!!!!!",
                  campaigner: "Yayasan Peduli Anak",
                ),
              ],
            ),
          )
        ],
      ),
    );
  }
}
