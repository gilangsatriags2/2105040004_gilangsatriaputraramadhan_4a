import 'package:flutter/material.dart';
import 'package:login_app_hci/views/theme.dart';
import 'package:login_app_hci/widgets/custom_appbar.dart';

class StatusPembayaranDompetDonasiPage extends StatefulWidget {
  const StatusPembayaranDompetDonasiPage({super.key});

  @override
  State<StatusPembayaranDompetDonasiPage> createState() =>
      _StatusPembayaranDompetDonasiPageState();
}

class _StatusPembayaranDompetDonasiPageState
    extends State<StatusPembayaranDompetDonasiPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: greenColor,
      appBar: CustomAppBar(
        title: "Cek Status Pembayaran",
        onBackButtonPressed: () {
          Navigator.pop(context);
        },
      ),
      body: Stack(
        children: [
          Column(
            children: <Widget>[
              SizedBox(
                height: 16,
              ),
              Text(
                "Pembayaran berhasil",
                style: TextStyle(
                  fontFamily: "PoppinsMedium",
                  fontSize: 18.0,
                  color: whiteColor,
                ),
              ),
              SizedBox(
                height: 10.0,
              ),
              Text(
                "Pembayaran dilakukan pada",
                style: TextStyle(
                  fontFamily: "PoppinsRegular",
                  fontSize: 14.0,
                  color: whiteColor,
                ),
              ),
              Text(
                "06 Desember 2022 11:50 WIB",
                style: TextStyle(
                  fontFamily: "PoppinsSB",
                  fontSize: 14.0,
                  color: whiteColor,
                ),
              ),
              SizedBox(
                height: 20,
              ),
              Container(
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(20.0),
                    topRight: Radius.circular(20.0),
                  ),
                  color: whiteColor,
                ),
                height: 600,
                width: MediaQuery.of(context).size.width,
                child: Column(
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.all(14.0),
                      child: Container(
                        decoration: BoxDecoration(
                          color: Color(0x0ffE6E6E6),
                          borderRadius: BorderRadius.circular(15.0),
                        ),
                        child: Padding(
                          padding: const EdgeInsets.all(18.0),
                          child: Column(
                            children: [
                              Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  Text(
                                    "Metode Pembayaran : ",
                                    style: TextStyle(
                                        fontFamily: "PoppinsLight",
                                        fontSize: 15,
                                        color: blackColor),
                                  ),
                                  Text(
                                    "Charity Dompet Donasi",
                                    style: TextStyle(
                                        fontFamily: "PoppinsMedium",
                                        fontSize: 15,
                                        color: blackColor),
                                  ),
                                ],
                              ),
                              SizedBox(
                                height: 3.0,
                              ),
                              Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  Text(
                                    "Kode Donasi : ",
                                    style: TextStyle(
                                        fontFamily: "PoppinsLight",
                                        fontSize: 15,
                                        color: blackColor),
                                  ),
                                  Text(
                                    "#9872139",
                                    style: TextStyle(
                                        fontFamily: "PoppinsMedium",
                                        fontSize: 15,
                                        color: blackColor),
                                  ),
                                ],
                              ),
                              SizedBox(
                                height: 3.0,
                              ),
                              Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  Text(
                                    "Waktu Bayar : ",
                                    style: TextStyle(
                                        fontFamily: "PoppinsLight",
                                        fontSize: 15,
                                        color: blackColor),
                                  ),
                                  Text(
                                    " 06-12-2022 11:50 WIB",
                                    style: TextStyle(
                                        fontFamily: "PoppinsMedium",
                                        fontSize: 15,
                                        color: blackColor),
                                  ),
                                ],
                              ),
                              SizedBox(
                                height: 3.0,
                              ),
                              Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  Text(
                                    "Waktu Bayar : ",
                                    style: TextStyle(
                                        fontFamily: "PoppinsLight",
                                        fontSize: 15,
                                        color: blackColor),
                                  ),
                                  Container(
                                    width: 92,
                                    height: 24,
                                    decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(10),
                                      border: Border.all(
                                        color: Color(0x0ff5EC6C6),
                                      ),
                                      color: Color(0x0ffD8FFFF),
                                    ),
                                    child: Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      children: [
                                        Text(
                                          "Sukses",
                                          style: TextStyle(
                                              fontFamily: "PoppinsRegular",
                                              fontSize: 12,
                                              color: Color(0x0ff5EC6C6)),
                                        ),
                                      ],
                                    ),
                                  ),
                                ],
                              ),
                            ],
                          ),
                        ),
                      ),
                    ),
                    TextButton(
                      onPressed: () {},
                      child: Container(
                        width: 385,
                        height: 42,
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(10),
                          color: Color(0x0ff5EC6C6),
                        ),
                        child: Align(
                          alignment: Alignment.center,
                          child: Text(
                            "Perbarui Status",
                            style: TextStyle(
                                fontFamily: "PoppinsRegular",
                                fontSize: 15.0,
                                color: whiteColor),
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }
}
