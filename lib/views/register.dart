import 'dart:math';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';

import 'login.dart';
import 'theme.dart';

class RegisterPage extends StatefulWidget {
  const RegisterPage({super.key});

  @override
  State<RegisterPage> createState() => _RegisterPageState();
}

class _RegisterPageState extends State<RegisterPage> {
  final TextEditingController emailController = TextEditingController();
  final TextEditingController nameController = TextEditingController();
  final TextEditingController phoneController = TextEditingController();
  final TextEditingController addressController = TextEditingController();
  final TextEditingController passwordController = TextEditingController();
  final TextEditingController passwordValController = TextEditingController();

  bool errorValidator = false;

  Future signUp() async {
    try {
      CollectionReference users =
          FirebaseFirestore.instance.collection('users');

      await FirebaseAuth.instance.createUserWithEmailAndPassword(
        email: emailController.text,
        password: passwordValController.text,
      );
      return users.add({
        'noAkun': generateUniqueNumber(),
        'full_name': nameController.text,
        'email': emailController.text,
        'phone': phoneController.text,
        'address': addressController.text,
        'imageUrl': "https://i.pravatar.cc/150?u=${nameController.text}",
        'bank': null,
        'noRek': null,
      });
      // await FirebaseAuth.instance.signOut();
    } on FirebaseAuthException catch (e) {
      if (e.code == 'weak-password') {
        print('The password provided is too weak.');
      } else if (e.code == 'email-already-in-use') {
        print('The account already exists for that email.');
      }
    } catch (e) {
      print(e);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: whiteColor,
      body: ListView(
        children: [
          Column(
            children: [
              Container(
                margin: EdgeInsets.only(
                  top: 96,
                  left: 29,
                  right: 30,
                ),
                child: Column(
                  children: [
                    Image.asset('images/Register.png'),
                  ],
                ),
              ),
              Container(
                padding: EdgeInsets.only(
                  left: 26,
                  right: 26,
                  top: 30,
                ),
                width: MediaQuery.of(context).size.width,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(30),
                    topRight: Radius.circular(30),
                  ),
                  color: secondaryWhiteColor,
                ),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Image.asset('images/LineProg.png'),
                    SizedBox(
                      height: 20,
                    ),
                    Text(
                      'Nama Lengkap',
                      style: TextStyle(
                        fontSize: 14,
                        fontFamily: 'PoppinsRegular',
                      ),
                      textAlign: TextAlign.start,
                    ),
                    SizedBox(
                      height: 9,
                    ),
                    SizedBox(
                      width: double.infinity,
                      height: 48,
                      child: TextFormField(
                        keyboardType: TextInputType.name,
                        textAlignVertical: TextAlignVertical.center,
                        controller: nameController,
                        decoration: InputDecoration(
                          border: OutlineInputBorder(
                            borderRadius: BorderRadius.all(Radius.circular(10)),
                            borderSide: BorderSide(
                              width: 0,
                              style: BorderStyle.none,
                            ),
                          ),
                          filled: true,
                          fillColor: greyColor,
                          hintText: 'Nama Lengkap',
                          hintStyle: TextStyle(
                            fontSize: 13,
                            color: blackColor,
                          ),
                          prefixIcon: Padding(
                            padding: EdgeInsets.only(
                              top: 10,
                              bottom: 9.76,
                              right: 18,
                              left: 28,
                            ),
                            child: Icon(
                              Icons.person,
                              color: greenColor,
                            ),
                          ),
                        ),
                      ),
                    ),
                    SizedBox(
                      height: 20,
                    ),
                    Text(
                      'Email',
                      style: TextStyle(
                        fontSize: 14,
                        fontFamily: 'PoppinsRegular',
                      ),
                      textAlign: TextAlign.start,
                    ),
                    SizedBox(
                      height: 9,
                    ),
                    SizedBox(
                      width: double.infinity,
                      height: 48,
                      child: TextFormField(
                        keyboardType: TextInputType.emailAddress,
                        controller: emailController,
                        textAlignVertical: TextAlignVertical.center,
                        decoration: InputDecoration(
                          border: OutlineInputBorder(
                            borderRadius: BorderRadius.all(Radius.circular(10)),
                            borderSide: BorderSide(
                              width: 0,
                              style: BorderStyle.none,
                            ),
                          ),
                          filled: true,
                          fillColor: greyColor,
                          hintText: 'Email',
                          hintStyle: TextStyle(
                            fontSize: 13,
                            color: blackColor,
                          ),
                          prefixIcon: Padding(
                            padding: EdgeInsets.only(
                              top: 10,
                              bottom: 9.76,
                              right: 18,
                              left: 30,
                            ),
                            child: Icon(
                              Icons.email,
                              color: greenColor,
                            ),
                          ),
                        ),
                      ),
                    ),
                    SizedBox(
                      height: 20,
                    ),
                    Text(
                      'Password',
                      style: TextStyle(
                        fontSize: 14,
                        fontFamily: 'PoppinsRegular',
                      ),
                    ),
                    SizedBox(
                      height: 3,
                    ),
                    Stack(
                      children: [
                        Container(
                          height: 48,
                          width: double.infinity,
                          child: TextFormField(
                            keyboardType: TextInputType.visiblePassword,
                            controller: passwordController,
                            obscureText: true,
                            textAlignVertical: TextAlignVertical.center,
                            decoration: InputDecoration(
                              border: OutlineInputBorder(
                                borderRadius:
                                    BorderRadius.all(Radius.circular(10)),
                                borderSide: BorderSide(
                                  width: 0,
                                  style: BorderStyle.none,
                                ),
                              ),
                              filled: true,
                              fillColor: greyColor,
                              hintText: 'Password',
                              hintStyle: TextStyle(
                                fontSize: 13,
                                color: blackColor,
                              ),
                              prefixIcon: Padding(
                                padding: EdgeInsets.only(
                                  top: 10,
                                  bottom: 9.76,
                                  right: 18,
                                  left: 30,
                                ),
                                child: Icon(
                                  Icons.lock,
                                  color: greenColor,
                                ),
                              ),
                            ),
                          ),
                        ),
                        Positioned(
                          bottom: 0,
                          left: 0,
                          child: Container(
                            padding: EdgeInsets.only(
                              left: 10,
                            ),
                            height: 16,
                            child: Text(
                              errorValidator ? "Password Tidak Sama!" : '',
                              style: TextStyle(
                                fontSize: 13,
                                color: Colors.red,
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                    SizedBox(
                      height: 20,
                    ),
                    Text(
                      'Validasi Password',
                      style: TextStyle(
                        fontSize: 14,
                        fontFamily: 'PoppinsRegular',
                      ),
                    ),
                    SizedBox(
                      height: 9,
                    ),
                    Stack(
                      children: [
                        Container(
                          height: 48,
                          width: double.infinity,
                          child: TextFormField(
                            controller: passwordValController,
                            keyboardType: TextInputType.visiblePassword,
                            obscureText: true,
                            textAlignVertical: TextAlignVertical.center,
                            decoration: InputDecoration(
                              border: OutlineInputBorder(
                                borderRadius:
                                    BorderRadius.all(Radius.circular(10)),
                                borderSide: BorderSide(
                                  width: 0,
                                  style: BorderStyle.none,
                                ),
                              ),
                              filled: true,
                              fillColor: greyColor,
                              hintText: 'Validasi Password',
                              hintStyle: TextStyle(
                                fontSize: 13,
                                color: blackColor,
                              ),
                              prefixIcon: Padding(
                                padding: EdgeInsets.only(
                                  top: 10,
                                  bottom: 9.76,
                                  right: 18,
                                  left: 30,
                                ),
                                child: Icon(
                                  Icons.lock,
                                  color: greenColor,
                                ),
                              ),
                            ),
                          ),
                        ),
                        Positioned(
                          bottom: 0,
                          left: 0,
                          child: Container(
                            padding: EdgeInsets.only(left: 10),
                            height: 16,
                            child: Text(
                              errorValidator ? "Password Tidak Sama!" : '',
                              style: TextStyle(
                                fontSize: 13,
                                color: Colors.red,
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                    SizedBox(
                      height: 20,
                    ),
                    Text(
                      'Nomor Telepon',
                      style: TextStyle(
                        fontSize: 14,
                        fontFamily: 'PoppinsRegular',
                      ),
                      textAlign: TextAlign.start,
                    ),
                    SizedBox(
                      height: 9,
                    ),
                    SizedBox(
                      width: double.infinity,
                      height: 48,
                      child: TextFormField(
                        keyboardType: TextInputType.phone,
                        textAlignVertical: TextAlignVertical.center,
                        controller: phoneController,
                        decoration: InputDecoration(
                          border: OutlineInputBorder(
                            borderRadius: BorderRadius.all(Radius.circular(10)),
                            borderSide: BorderSide(
                              width: 0,
                              style: BorderStyle.none,
                            ),
                          ),
                          filled: true,
                          fillColor: greyColor,
                          hintText: 'Phone',
                          hintStyle: TextStyle(
                            fontSize: 13,
                            color: blackColor,
                          ),
                          prefixIcon: Padding(
                            padding: EdgeInsets.only(
                              top: 10,
                              bottom: 9.76,
                              right: 18,
                              left: 30,
                            ),
                            child: Icon(
                              Icons.phone,
                              color: greenColor,
                            ),
                          ),
                        ),
                      ),
                    ),
                    SizedBox(
                      height: 20,
                    ),
                    Text(
                      'Alamat',
                      style: TextStyle(
                        fontSize: 14,
                        fontFamily: 'PoppinsRegular',
                      ),
                      textAlign: TextAlign.start,
                    ),
                    SizedBox(
                      height: 9,
                    ),
                    Container(
                      width: double.infinity,
                      height: 120, // Atur tinggi TextArea sesuai kebutuhan Anda
                      child: TextField(
                        controller: addressController,
                        keyboardType: TextInputType.multiline,
                        maxLines:
                            null, // Set maxLines menjadi null agar bisa memiliki beberapa baris
                        textAlignVertical:
                            TextAlignVertical.top, // Set posisi teks ke atas
                        decoration: InputDecoration(
                          border: OutlineInputBorder(
                            borderRadius: BorderRadius.all(Radius.circular(10)),
                            borderSide: BorderSide(
                              width: 0,
                              style: BorderStyle.none,
                            ),
                          ),
                          filled: true,
                          fillColor: greyColor,
                          hintText: 'Alamat',
                          hintStyle: TextStyle(
                            fontSize: 13,
                            color: blackColor,
                          ),
                          prefixIcon: Padding(
                            padding: EdgeInsets.only(
                              top: 10,
                              bottom: 9.76,
                              right: 18,
                              left: 28,
                            ),
                            child: Image.asset(
                              'images/User.png',
                            ),
                          ),
                        ),
                      ),
                    ),
                    SizedBox(
                      height: 5,
                    ),
                    Image.asset("images/Line.png"),
                    SizedBox(
                      height: 30,
                    ),
                    SizedBox(
                      width: double.infinity,
                      height: 48,
                      child: ElevatedButton(
                        style: ElevatedButton.styleFrom(
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(10.0),
                          ),
                          backgroundColor: greenColor,
                        ),
                        onPressed: () {
                          setState(() {
                            passwordController.text ==
                                    passwordValController.text
                                ? errorValidator = false
                                : errorValidator = true;
                          });
                          if (errorValidator) {
                            print("error");
                          } else {
                            signUp();

                            Navigator.pop(context);
                          }
                        },
                        child: Text(
                          "Daftar",
                          style: TextStyle(
                            color: whiteColor,
                            fontFamily: "PoppinsMedium",
                            fontSize: 14,
                          ),
                        ),
                      ),
                    ),
                    SizedBox(
                      height: 25,
                    ),
                    SizedBox(
                      width: double.infinity,
                      child: InkWell(
                        child: RichText(
                          textAlign: TextAlign.center,
                          text: TextSpan(
                            text: 'Kamu sudah punya akun ? ',
                            style: TextStyle(
                              fontFamily: "PoppinsRegular",
                              fontSize: 14,
                              color: blackColor,
                            ),
                            children: <InlineSpan>[
                              TextSpan(
                                text: 'Login disini',
                                style: TextStyle(
                                  fontFamily: "PoppinsRegular",
                                  fontSize: 14,
                                  color: greenColor,
                                ),
                                recognizer: TapGestureRecognizer()
                                  ..onTap = () {
                                    Navigator.push(
                                      context,
                                      MaterialPageRoute(
                                        builder: (context) => LoginPage(),
                                      ),
                                    );
                                  },
                              ),
                            ],
                          ),
                        ),
                      ),
                    ),
                    SizedBox(
                      height: 20,
                    ),
                  ],
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }

  int generateUniqueNumber() {
    Random random = Random();
    int timestamp = DateTime.now().millisecondsSinceEpoch;
    int randomNumber = random.nextInt(999999);
    int uniqueNumber = int.parse('$timestamp$randomNumber');
    return uniqueNumber;
  }
}
