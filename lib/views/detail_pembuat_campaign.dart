import 'package:expandable_text/expandable_text.dart';
import 'package:flutter/material.dart';
import 'package:login_app_hci/widgets/custom_appbar.dart';
import 'package:login_app_hci/widgets/list_campaign.dart';

import 'theme.dart';

class DetailPembuatCampaignPage extends StatefulWidget {
  const DetailPembuatCampaignPage({super.key});

  @override
  State<DetailPembuatCampaignPage> createState() =>
      _DetailPembuatCampaignPageState();
}

class _DetailPembuatCampaignPageState extends State<DetailPembuatCampaignPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: CustomAppBar(
        title: "Tentang Pembuat Campaign",
        onBackButtonPressed: () {
          Navigator.pop(context);
        },
      ),
      backgroundColor: whiteColor,
      body: ListView(
        children: <Widget>[
          SizedBox(
            height: 6.0,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Image.asset("images/ypl_xl.png"),
              SizedBox(
                width: 10.0,
              ),
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Row(
                    children: <Widget>[
                      Text(
                        "Yayasan Peduli Lansia",
                        style: TextStyle(
                          fontFamily: "PoppinsMedium",
                          fontSize: 16.0,
                          color: blackColor,
                        ),
                      ),
                      SizedBox(
                        width: 4.0,
                      ),
                      Image.asset("images/icons/check.png"),
                    ],
                  ),
                  SizedBox(
                    height: 3.0,
                  ),
                  Text(
                    "Aktif sejak 15 November 2021",
                    style: TextStyle(
                      fontFamily: "PoppinsRegular",
                      fontSize: 14.0,
                      color: Color(0x0ff7E7E7E),
                    ),
                  ),
                ],
              ),
            ],
          ),
          Container(
            margin: EdgeInsets.symmetric(horizontal: 19.0, vertical: 16.0),
            width: 390.0,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(20),
              color: Color(0x0ffe8e8e8),
            ),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Padding(
                  padding: const EdgeInsets.only(top: 14.0, left: 19.0),
                  child: Text(
                    "Tentang Campaign Donasi",
                    style: TextStyle(
                      fontFamily: "PoppinsSB",
                      fontSize: 13.0,
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(
                      top: 2.0, left: 27.0, right: 20.0, bottom: 14.0),
                  child: ExpandableText(
                    "Yayasan Peduli Lansia adalah yayasan memberdayakan dan membantu lansia yang ditelantarkan. Kami juga memiliki program untuk  membuat sebuah panti jompo. Panti jompo ini nantinya digunakan oleh para lansia yang terlantar untuk dijadikan tempat tinggal yang nyaman dan aman",
                    expandText: 'lanjutkan membaca',
                    linkColor: greenColor,
                    collapseText: 'lebih sedikit',
                    maxLines: 5,
                    style: TextStyle(
                      fontFamily: "PoppinsRegular",
                      fontSize: 13.0,
                      color: blackColor,
                    ),
                  ),
                ),
              ],
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(left: 19.0, right: 9),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  "Riwayat Campaign Donasi",
                  style: TextStyle(
                    fontFamily: "PoppinsRegular",
                    fontSize: 15.0,
                    color: blackColor,
                  ),
                ),
                SizedBox(
                  height: 7.0,
                ),
                ListCampaign(
                  image: "paul",
                  widthPicture: 145.0,
                  campaigner: "Yayasan Peduli Lansia",
                  heightPicture: 82.0,
                  campaign: "Bantu kakek paul untuk mendapatkan tempat tinggal",
                ),
                ListCampaign(
                  image: "lansia",
                  widthPicture: 145.0,
                  campaigner: "Yayasan Peduli Lansia",
                  heightPicture: 82.0,
                  campaign:
                      "Ayo Bantu Para Lansia Terlantara di Wilayah Magelang",
                ),
                ListCampaign(
                  image: "lansia",
                  widthPicture: 145.0,
                  campaigner: "Yayasan Peduli Lansia",
                  heightPicture: 82.0,
                  campaign:
                      "Ayo Bantu Para Lansia Terlantara di Wilayah Magelang",
                ),
                ListCampaign(
                  image: "lansia",
                  widthPicture: 145.0,
                  campaigner: "Yayasan Peduli Lansia",
                  campaign:
                      "Ayo Bantu Para Lansia Terlantara di Wilayah Magelang",
                  heightPicture: 82.0,
                ),
                ListCampaign(
                  image: "lansia",
                  widthPicture: 145.0,
                  campaigner: "Yayasan Peduli Lansia",
                  campaign:
                      "Ayo Bantu Para Lansia Terlantara di Wilayah Magelang",
                  heightPicture: 82.0,
                ),
                ListCampaign(
                  image: "lansia",
                  widthPicture: 145.0,
                  campaigner: "Yayasan Peduli Lansia",
                  campaign:
                      "Ayo Bantu Para Lansia Terlantara di Wilayah Magelang",
                  heightPicture: 82.0,
                ),
                ListCampaign(
                  image: "lansia",
                  widthPicture: 145.0,
                  campaigner: "Yayasan Peduli Lansia",
                  campaign:
                      "Ayo Bantu Para Lansia Terlantara di Wilayah Magelang",
                  heightPicture: 82.0,
                ),
                SizedBox(
                  height: 10.0,
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
