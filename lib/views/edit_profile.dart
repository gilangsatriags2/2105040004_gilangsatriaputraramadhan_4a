import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:login_app_hci/views/theme.dart';

import 'edit_profile_data.dart';

class EditProfile extends StatelessWidget {
  final String docId;

  const EditProfile({Key? key, required this.docId}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        automaticallyImplyLeading: false,
        title: Center(
          child: Container(
            child: Text("Edit Profile"),
          ),
        ),
        backgroundColor: greenColor,
        elevation: 0,
      ),
      body: EditProfileData(docId: docId),
    );
  }
}
