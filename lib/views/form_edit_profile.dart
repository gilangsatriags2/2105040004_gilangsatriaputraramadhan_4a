import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:login_app_hci/views/profile.dart';

import 'theme.dart';

class EditProfileForm extends StatefulWidget {
  final Map<String, dynamic> data;
  final String docId;

  EditProfileForm({required this.data, required this.docId});

  @override
  State<EditProfileForm> createState() => _EditProfileFormState();
}

class _EditProfileFormState extends State<EditProfileForm> {
  final TextEditingController addressController = TextEditingController();
  final TextEditingController phoneController = TextEditingController();
  final TextEditingController emailController = TextEditingController();
  final TextEditingController bankController = TextEditingController();
  final TextEditingController nameController = TextEditingController();
  final TextEditingController noRekController = TextEditingController();
  FirebaseFirestore firestore = FirebaseFirestore.instance;
  CollectionReference users = FirebaseFirestore.instance.collection('users');

  Future<void> updateData(String docId, Map<String, dynamic> newData) async {
    try {
      await users.doc(docId).update(newData);
      print("Data updated successfully");
    } catch (e) {
      print("Error updating data: $e");
    }
  }

  @override
  void initState() {
    super.initState();
    addressController.text = widget.data['address'] ?? '';
    phoneController.text = widget.data['phone'] ?? '';
    emailController.text = widget.data['email'] ?? '';
    bankController.text = widget.data['bank'] ?? '';
    nameController.text = widget.data['full_name'] ?? '';
    noRekController.text = widget.data['noRek'] ?? '';
  }

  @override
  Widget build(BuildContext context) {
    return ListView(
      children: [
        Column(
          children: [
            Container(
              padding: EdgeInsets.only(
                left: 26,
                right: 26,
                top: 30,
              ),
              width: MediaQuery.of(context).size.width,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(30),
                  topRight: Radius.circular(30),
                ),
                color: secondaryWhiteColor,
              ),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  SizedBox(
                    height: 20,
                  ),
                  Text(
                    'Nama Lengkap',
                    style: TextStyle(
                      fontSize: 14,
                      fontFamily: 'PoppinsRegular',
                    ),
                    textAlign: TextAlign.start,
                  ),
                  SizedBox(
                    height: 9,
                  ),
                  SizedBox(
                    width: double.infinity,
                    height: 48,
                    child: TextFormField(
                      keyboardType: TextInputType.name,
                      textAlignVertical: TextAlignVertical.center,
                      controller: nameController,
                      decoration: InputDecoration(
                        border: OutlineInputBorder(
                          borderRadius: BorderRadius.all(Radius.circular(10)),
                          borderSide: BorderSide(
                            width: 0,
                            style: BorderStyle.none,
                          ),
                        ),
                        filled: true,
                        fillColor: greyColor,
                        hintText: 'Nama Lengkap',
                        hintStyle: TextStyle(
                          fontSize: 13,
                          color: blackColor,
                        ),
                        prefixIcon: Padding(
                          padding: EdgeInsets.only(
                            top: 10,
                            bottom: 9.76,
                            right: 18,
                            left: 28,
                          ),
                          child: Icon(
                            Icons.person,
                            color: greenColor,
                          ),
                        ),
                      ),
                    ),
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  Text(
                    'Email',
                    style: TextStyle(
                      fontSize: 14,
                      fontFamily: 'PoppinsRegular',
                    ),
                    textAlign: TextAlign.start,
                  ),
                  SizedBox(
                    height: 9,
                  ),
                  SizedBox(
                    width: double.infinity,
                    height: 48,
                    child: TextFormField(
                      keyboardType: TextInputType.emailAddress,
                      controller: emailController,
                      textAlignVertical: TextAlignVertical.center,
                      decoration: InputDecoration(
                        border: OutlineInputBorder(
                          borderRadius: BorderRadius.all(Radius.circular(10)),
                          borderSide: BorderSide(
                            width: 0,
                            style: BorderStyle.none,
                          ),
                        ),
                        filled: true,
                        fillColor: greyColor,
                        hintText: 'Email',
                        hintStyle: TextStyle(
                          fontSize: 13,
                          color: blackColor,
                        ),
                        prefixIcon: Padding(
                          padding: EdgeInsets.only(
                            top: 10,
                            bottom: 9.76,
                            right: 18,
                            left: 30,
                          ),
                          child: Icon(
                            Icons.email,
                            color: greenColor,
                          ),
                        ),
                      ),
                    ),
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  Text(
                    'Bank',
                    style: TextStyle(
                      fontSize: 14,
                      fontFamily: 'PoppinsRegular',
                    ),
                    textAlign: TextAlign.start,
                  ),
                  SizedBox(
                    height: 9,
                  ),
                  SizedBox(
                    width: double.infinity,
                    height: 48,
                    child: TextFormField(
                      keyboardType: TextInputType.name,
                      textAlignVertical: TextAlignVertical.center,
                      controller: bankController,
                      decoration: InputDecoration(
                        border: OutlineInputBorder(
                          borderRadius: BorderRadius.all(Radius.circular(10)),
                          borderSide: BorderSide(
                            width: 0,
                            style: BorderStyle.none,
                          ),
                        ),
                        filled: true,
                        fillColor: greyColor,
                        hintText: 'Bank',
                        hintStyle: TextStyle(
                          fontSize: 13,
                          color: blackColor,
                        ),
                        prefixIcon: Padding(
                          padding: EdgeInsets.only(
                            top: 10,
                            bottom: 9.76,
                            right: 18,
                            left: 28,
                          ),
                          child: Icon(
                            Icons.home,
                            color: greenColor,
                          ),
                        ),
                      ),
                    ),
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  Text(
                    'Nomor Rekening',
                    style: TextStyle(
                      fontSize: 14,
                      fontFamily: 'PoppinsRegular',
                    ),
                    textAlign: TextAlign.start,
                  ),
                  SizedBox(
                    height: 9,
                  ),
                  SizedBox(
                    width: double.infinity,
                    height: 48,
                    child: TextFormField(
                      keyboardType: TextInputType.number,
                      textAlignVertical: TextAlignVertical.center,
                      controller: noRekController,
                      decoration: InputDecoration(
                        border: OutlineInputBorder(
                          borderRadius: BorderRadius.all(Radius.circular(10)),
                          borderSide: BorderSide(
                            width: 0,
                            style: BorderStyle.none,
                          ),
                        ),
                        filled: true,
                        fillColor: greyColor,
                        hintText: 'Nomor Rekening',
                        hintStyle: TextStyle(
                          fontSize: 13,
                          color: blackColor,
                        ),
                        prefixIcon: Padding(
                          padding: EdgeInsets.only(
                            top: 10,
                            bottom: 9.76,
                            right: 18,
                            left: 28,
                          ),
                          child: Icon(
                            Icons.numbers,
                            color: greenColor,
                          ),
                        ),
                      ),
                    ),
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  Text(
                    'Nomor Telepon',
                    style: TextStyle(
                      fontSize: 14,
                      fontFamily: 'PoppinsRegular',
                    ),
                    textAlign: TextAlign.start,
                  ),
                  SizedBox(
                    height: 9,
                  ),
                  SizedBox(
                    width: double.infinity,
                    height: 48,
                    child: TextFormField(
                      keyboardType: TextInputType.phone,
                      textAlignVertical: TextAlignVertical.center,
                      controller: phoneController,
                      decoration: InputDecoration(
                        border: OutlineInputBorder(
                          borderRadius: BorderRadius.all(Radius.circular(10)),
                          borderSide: BorderSide(
                            width: 0,
                            style: BorderStyle.none,
                          ),
                        ),
                        filled: true,
                        fillColor: greyColor,
                        hintText: 'Phone',
                        hintStyle: TextStyle(
                          fontSize: 13,
                          color: blackColor,
                        ),
                        prefixIcon: Padding(
                          padding: EdgeInsets.only(
                            top: 10,
                            bottom: 9.76,
                            right: 18,
                            left: 30,
                          ),
                          child: Icon(
                            Icons.phone,
                            color: greenColor,
                          ),
                        ),
                      ),
                    ),
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  Text(
                    'Alamat',
                    style: TextStyle(
                      fontSize: 14,
                      fontFamily: 'PoppinsRegular',
                    ),
                    textAlign: TextAlign.start,
                  ),
                  SizedBox(
                    height: 9,
                  ),
                  Container(
                    width: double.infinity,
                    height: 120, // Atur tinggi TextArea sesuai kebutuhan Anda
                    child: TextField(
                      controller: addressController,
                      keyboardType: TextInputType.multiline,
                      maxLines:
                          null, // Set maxLines menjadi null agar bisa memiliki beberapa baris
                      textAlignVertical:
                          TextAlignVertical.top, // Set posisi teks ke atas
                      decoration: InputDecoration(
                        border: OutlineInputBorder(
                          borderRadius: BorderRadius.all(Radius.circular(10)),
                          borderSide: BorderSide(
                            width: 0,
                            style: BorderStyle.none,
                          ),
                        ),
                        filled: true,
                        fillColor: greyColor,
                        hintText: 'Alamat',
                        hintStyle: TextStyle(
                          fontSize: 13,
                          color: blackColor,
                        ),
                        prefixIcon: Padding(
                          padding: EdgeInsets.only(
                            top: 10,
                            bottom: 9.76,
                            right: 18,
                            left: 28,
                          ),
                          child: Icon(
                            Icons.location_on,
                            color: greenColor,
                          ),
                        ),
                      ),
                    ),
                  ),
                  TextButton(
                    onPressed: () {
                      Map<String, dynamic> newData = {
                        "full_name": nameController.text,
                        "address": addressController.text,
                        "email": emailController.text,
                        "bank": bankController.text,
                        "noRek": noRekController.text,
                        "phone": phoneController.text,
                      };
                      updateData(widget.docId, newData);
                      Navigator.pushReplacement(
                          context,
                          MaterialPageRoute(
                              builder: (context) =>
                                  ProfilePage(docId: widget.docId)));
                    },
                    child: Text("Edit"),
                  ),
                ],
              ),
            ),
          ],
        ),
      ],
    );
  }
}
