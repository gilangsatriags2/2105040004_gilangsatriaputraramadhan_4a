import 'package:flutter/material.dart';

const Color greenColor = const Color(0xFF56BFBF);
const Color greyColor = const Color(0xFFE3E3E3);
const Color whiteColor = const Color(0xFFFFFFFF);
const Color secondaryWhiteColor = const Color(0xFFF4F4F4);
const Color blackColor = const Color(0xFF000000);
