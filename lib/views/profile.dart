import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:loading_animation_widget/loading_animation_widget.dart';
import 'package:login_app_hci/views/edit_profile.dart';
import 'package:login_app_hci/views/theme.dart';

import '../widgets/dompet_donasi.dart';
import '../widgets/layanan.dart';
import '../widgets/riwayat_campaign_donasi.dart';
import '../widgets/riwayat_dompet_donasi.dart';
import '../widgets/riwayat_donasi.dart';
import '../widgets/tentang.dart';
import 'home.dart';

class ProfilePage extends StatefulWidget {
  const ProfilePage({super.key, this.docId});
  final String? docId;

  @override
  State<ProfilePage> createState() => _ProfilePageState();
}

class _ProfilePageState extends State<ProfilePage> {
  String activeTab = "Personal";
  double _kSize = 100;
  FirebaseFirestore firestore = FirebaseFirestore.instance;
  FirebaseAuth auth = FirebaseAuth.instance;
  User? user;
  CollectionReference users = FirebaseFirestore.instance.collection('users');

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Container(
          margin: EdgeInsets.only(left: 105.0),
          child: Text("Profile"),
        ),
        leading: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Container(
            width: 38.0,
            height: 38.0,
            decoration: BoxDecoration(
                color: whiteColor, borderRadius: BorderRadius.circular(100)),
            child: IconButton(
              onPressed: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => HomePage(),
                  ),
                );
              },
              icon: Icon(Icons.arrow_back_ios_new_rounded),
              color: blackColor,
            ),
          ),
        ),
        backgroundColor: greenColor,
        elevation: 0,
      ),
      body: FutureBuilder(
        future: users.doc(widget.docId).get(),
        builder:
            (BuildContext context, AsyncSnapshot<DocumentSnapshot> snapshot) {
          if (snapshot.hasError) {
            return Text("Something went wrong");
          }
          if (snapshot.hasData && !snapshot.data!.exists) {
            return Text("Document does not exist");
          }
          if (snapshot.connectionState == ConnectionState.done) {
            Map<String, dynamic> data =
                snapshot.data!.data() as Map<String, dynamic>;
            return ListView(
              children: [
                Column(
                  children: [
                    Stack(
                      children: [
                        Container(
                          decoration: BoxDecoration(color: greenColor),
                          height: 180.0,
                        ),
                        Padding(
                          padding: const EdgeInsets.symmetric(horizontal: 17.0),
                          child: Column(
                            children: [
                              Padding(
                                padding: const EdgeInsets.symmetric(
                                    horizontal: 13.0),
                                child: Column(
                                  children: [
                                    Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceBetween,
                                      children: [
                                        Container(
                                          width: 250,
                                          child: Column(
                                            crossAxisAlignment:
                                                CrossAxisAlignment.start,
                                            children: [
                                              Text(
                                                "${data['full_name']}",
                                                style: TextStyle(
                                                  fontFamily: "PoppinsSB",
                                                  fontSize: 20.0,
                                                  color: whiteColor,
                                                ),
                                              ),
                                              Text(
                                                "${data['address']}",
                                                style: TextStyle(
                                                  fontFamily: "PoppinsRegular",
                                                  fontSize: 15.0,
                                                  color: whiteColor,
                                                ),
                                              ),
                                            ],
                                          ),
                                        ),
                                        CircleAvatar(
                                          backgroundImage: NetworkImage(
                                            "${data['imageUrl']}",
                                          ),
                                          radius: 45.0,
                                        ),
                                      ],
                                    ),
                                    SizedBox(
                                      height: 5.0,
                                    ),
                                    Row(
                                      children: [
                                        Column(
                                          children: [
                                            InkWell(
                                              onTap: () {
                                                setState(() {
                                                  activeTab = "Personal";
                                                });
                                              },
                                              child: Text(
                                                "Personal",
                                                style: TextStyle(
                                                  fontFamily: "PoppinsRegular",
                                                  fontSize: 15.0,
                                                  color: whiteColor,
                                                ),
                                              ),
                                            ),
                                            if (activeTab == "Personal")
                                              Container(
                                                decoration: BoxDecoration(
                                                  borderRadius:
                                                      BorderRadius.circular(
                                                          100),
                                                ),
                                                width: 67.0,
                                                child: const Divider(
                                                  thickness: 4.0,
                                                  color: whiteColor,
                                                ),
                                              ),
                                            if (activeTab != "Personal")
                                              Container(
                                                decoration: BoxDecoration(
                                                  borderRadius:
                                                      BorderRadius.circular(
                                                          100),
                                                ),
                                                width: 67.0,
                                                child: const Divider(
                                                  thickness: 4.0,
                                                  color: greenColor,
                                                ),
                                              ),
                                          ],
                                        ),
                                        SizedBox(
                                          width: 12.0,
                                        ),
                                        Column(
                                          children: [
                                            InkWell(
                                              onTap: () {
                                                setState(() {
                                                  activeTab = "Riwayat";
                                                });
                                              },
                                              child: Text(
                                                "Riwayat",
                                                style: TextStyle(
                                                  fontFamily: "PoppinsRegular",
                                                  fontSize: 15.0,
                                                  color: whiteColor,
                                                ),
                                              ),
                                            ),
                                            if (activeTab == "Riwayat")
                                              Container(
                                                decoration: BoxDecoration(
                                                  borderRadius:
                                                      BorderRadius.circular(
                                                          100),
                                                ),
                                                width: 67.0,
                                                child: const Divider(
                                                  thickness: 4.0,
                                                  color: whiteColor,
                                                ),
                                              ),
                                            if (activeTab != "Riwayat")
                                              Container(
                                                decoration: BoxDecoration(
                                                  borderRadius:
                                                      BorderRadius.circular(
                                                          100),
                                                ),
                                                width: 67.0,
                                                child: const Divider(
                                                  thickness: 4.0,
                                                  color: greenColor,
                                                ),
                                              ),
                                          ],
                                        ),
                                      ],
                                    ),
                                  ],
                                ),
                              ),
                              if (activeTab == "Personal")
                                Visibility(
                                  visible: activeTab == "Personal",
                                  child: Column(
                                    children: <Widget>[
                                      DompetDonasi(),
                                      Tentang(
                                        email: "${data['email']}",
                                        phone: "${data['phone']}",
                                        noRek: "${data['noRek']}",
                                        bank: "${data['bank']}",
                                        noAkun: data['noAkun'],
                                        onpressed: () {
                                          Navigator.push(
                                              context,
                                              MaterialPageRoute(
                                                  builder: (context) =>
                                                      EditProfile(
                                                          docId:
                                                              widget.docId!)));
                                        },
                                      ),
                                      Layanan(),
                                      SizedBox(
                                        height: 50,
                                      ),
                                    ],
                                  ),
                                ),
                              if (activeTab == "Riwayat")
                                Visibility(
                                  visible: activeTab == "Riwayat",
                                  child: Column(
                                    children: <Widget>[
                                      RiwayatDompetDonasi(),
                                      RiwayatDonasi(),
                                      RiwayatCampaignDonasi(),
                                    ],
                                  ),
                                ),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
              ],
            );
          }
          return Container(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Center(
                  child: LoadingAnimationWidget.waveDots(
                    color: Color.fromRGBO(158, 158, 158, 0.25),
                    size: _kSize,
                  ),
                ),
              ],
            ),
          );
        },
      ),
    );
  }
}

// Scaffold(
//       appBar: AppBar(
//         title: Container(
//           margin: EdgeInsets.only(left: 105.0),
//           child: Text("Profile"),
//         ),
//         leading: Padding(
//           padding: const EdgeInsets.all(8.0),
//           child: Container(
//             width: 38.0,
//             height: 38.0,
//             decoration: BoxDecoration(
//                 color: whiteColor, borderRadius: BorderRadius.circular(100)),
//             child: IconButton(
//               onPressed: () {
//                 Navigator.push(
//                   context,
//                   MaterialPageRoute(
//                     builder: (context) => HomePage(),
//                   ),
//                 );
//               },
//               icon: Icon(Icons.arrow_back_ios_new_rounded),
//               color: blackColor,
//             ),
//           ),
//         ),
//         backgroundColor: greenColor,
//         elevation: 0,
//       ),
//       body: ListView(
//         children: [
//           Column(
//             children: [
//               Stack(
//                 children: [
//                   Container(
//                     decoration: BoxDecoration(color: greenColor),
//                     height: 180.0,
//                   ),
//                   Padding(
//                     padding: const EdgeInsets.symmetric(horizontal: 17.0),
//                     child: Column(
//                       children: [
//                         Padding(
//                           padding: const EdgeInsets.symmetric(horizontal: 13.0),
//                           child: Column(
//                             children: [
//                               Row(
//                                 mainAxisAlignment:
//                                     MainAxisAlignment.spaceBetween,
//                                 children: [
//                                   Column(
//                                     crossAxisAlignment:
//                                         CrossAxisAlignment.start,
//                                     children: [
//                                       Text(
//                                         "Suzie Poo Margareth",
//                                         style: TextStyle(
//                                           fontFamily: "PoppinsSB",
//                                           fontSize: 20.0,
//                                           color: whiteColor,
//                                         ),
//                                       ),
//                                       Text(
//                                         "Kab. Magelang, Indonesia",
//                                         style: TextStyle(
//                                           fontFamily: "PoppinsRegular",
//                                           fontSize: 15.0,
//                                           color: whiteColor,
//                                         ),
//                                       ),
//                                     ],
//                                   ),
//                                   CircleAvatar(
//                                     backgroundImage: NetworkImage(
//                                       "https://api.multiavatar.com/Starcrasher.png",
//                                     ),
//                                     radius: 45.0,
//                                   ),
//                                 ],
//                               ),
//                               SizedBox(
//                                 height: 5.0,
//                               ),
//                               Row(
//                                 children: [
//                                   Column(
//                                     children: [
//                                       InkWell(
//                                         onTap: () {
//                                           setState(() {
//                                             activeTab = "Personal";
//                                           });
//                                         },
//                                         child: Text(
//                                           "Personal",
//                                           style: TextStyle(
//                                             fontFamily: "PoppinsRegular",
//                                             fontSize: 15.0,
//                                             color: whiteColor,
//                                           ),
//                                         ),
//                                       ),
//                                       if (activeTab == "Personal")
//                                         Container(
//                                           decoration: BoxDecoration(
//                                             borderRadius:
//                                                 BorderRadius.circular(100),
//                                           ),
//                                           width: 67.0,
//                                           child: const Divider(
//                                             thickness: 4.0,
//                                             color: whiteColor,
//                                           ),
//                                         ),
//                                       if (activeTab != "Personal")
//                                         Container(
//                                           decoration: BoxDecoration(
//                                             borderRadius:
//                                                 BorderRadius.circular(100),
//                                           ),
//                                           width: 67.0,
//                                           child: const Divider(
//                                             thickness: 4.0,
//                                             color: greenColor,
//                                           ),
//                                         ),
//                                     ],
//                                   ),
//                                   SizedBox(
//                                     width: 12.0,
//                                   ),
//                                   Column(
//                                     children: [
//                                       InkWell(
//                                         onTap: () {
//                                           setState(() {
//                                             activeTab = "Riwayat";
//                                           });
//                                         },
//                                         child: Text(
//                                           "Riwayat",
//                                           style: TextStyle(
//                                             fontFamily: "PoppinsRegular",
//                                             fontSize: 15.0,
//                                             color: whiteColor,
//                                           ),
//                                         ),
//                                       ),
//                                       if (activeTab == "Riwayat")
//                                         Container(
//                                           decoration: BoxDecoration(
//                                             borderRadius:
//                                                 BorderRadius.circular(100),
//                                           ),
//                                           width: 67.0,
//                                           child: const Divider(
//                                             thickness: 4.0,
//                                             color: whiteColor,
//                                           ),
//                                         ),
//                                       if (activeTab != "Riwayat")
//                                         Container(
//                                           decoration: BoxDecoration(
//                                             borderRadius:
//                                                 BorderRadius.circular(100),
//                                           ),
//                                           width: 67.0,
//                                           child: const Divider(
//                                             thickness: 4.0,
//                                             color: greenColor,
//                                           ),
//                                         ),
//                                     ],
//                                   ),
//                                 ],
//                               ),
//                             ],
//                           ),
//                         ),
//                         if (activeTab == "Personal")
//                           Visibility(
//                             visible: activeTab == "Personal",
//                             child: Column(
//                               children: <Widget>[
//                                 DompetDonasi(),
//                                 Tentang(),
//                                 Layanan(),
//                                 SizedBox(
//                                   height: 50,
//                                 ),
//                               ],
//                             ),
//                           ),
//                         if (activeTab == "Riwayat")
//                           Visibility(
//                             visible: activeTab == "Riwayat",
//                             child: Column(
//                               children: <Widget>[
//                                 RiwayatDompetDonasi(),
//                                 RiwayatDonasi(),
//                                 RiwayatCampaignDonasi(),
//                               ],
//                             ),
//                           ),
//                       ],
//                     ),
//                   ),
//                 ],
//               ),
//             ],
//           ),
//         ],
//       ),
//     );