import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:login_app_hci/views/theme.dart';
import 'package:login_app_hci/widgets/custom_appbar.dart';
import 'package:login_app_hci/widgets/list_campaign.dart';

class KategoriPage extends StatefulWidget {
  final String kategori;
  const KategoriPage({super.key, required this.kategori});

  @override
  State<KategoriPage> createState() => _KategoriPageState();
}

class _KategoriPageState extends State<KategoriPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: CustomAppBar(
        title: "${widget.kategori}",
        onBackButtonPressed: () {
          Navigator.pop(context);
        },
      ),
      body: ListView(
        children: <Widget>[
          SizedBox(
            height: 23.0,
          ),
          Padding(
            padding: const EdgeInsets.only(left: 16.0, right: 12),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  "Campaign donasi dengan kategori ${widget.kategori}",
                  style: TextStyle(
                    fontFamily: "PoppinsRegular",
                    fontSize: 15.0,
                    color: blackColor,
                  ),
                ),
                SizedBox(
                  height: 9.0,
                ),
                Padding(
                  padding: const EdgeInsets.only(bottom: 10.0),
                  child: Column(
                    children: [
                      ListCampaign(
                        image: "campaign",
                        widthPicture: 145.0,
                        heightPicture: 82.0,
                        campaigner: "Yayasan Peduli Anak",
                        campaign:
                            "Banyak anak kecil terlantar di Bandung, Ayo Bantu !!!!!!!!!",
                      ),
                      ListCampaign(
                        image: "lansia",
                        widthPicture: 145.0,
                        heightPicture: 82.0,
                        campaign:
                            "Ayo Bantu Para Lansia Terlantara di Wilayah Magelang  ",
                        campaigner: "Yayasan Peduli Lansia",
                      ),
                      ListCampaign(
                        image: "paul",
                        widthPicture: 145.0,
                        heightPicture: 82.0,
                        campaign:
                            "Bantu kakek paul untuk mendapatkan tempat tinggal",
                        campaigner: "Yayasan Peduli Lansia",
                      ),
                      ListCampaign(
                        image: "campaign",
                        widthPicture: 145.0,
                        heightPicture: 82.0,
                        campaign:
                            "Banyak anak kecil terlantar di Bandung, Ayo Bantu !!!!!!!!!",
                        campaigner: "Yayasan Peduli Anak",
                      ),
                      ListCampaign(
                        image: "lansia",
                        widthPicture: 145.0,
                        heightPicture: 82.0,
                        campaign:
                            "Ayo Bantu Para Lansia Terlantara di Wilayah Magelang  ",
                        campaigner: "Yayasan Peduli Lansia",
                      ),
                      ListCampaign(
                        image: "paul",
                        widthPicture: 145.0,
                        heightPicture: 82.0,
                        campaign:
                            "Bantu kakek paul untuk mendapatkan tempat tinggal",
                        campaigner: "Yayasan Peduli Lansia",
                      ),
                      ListCampaign(
                        image: "campaign",
                        widthPicture: 145.0,
                        heightPicture: 82.0,
                        campaign:
                            "Banyak anak kecil terlantar di Bandung, Ayo Bantu !!!!!!!!!",
                        campaigner: "Yayasan Peduli Anak",
                      ),
                      ListCampaign(
                        image: "lansia",
                        widthPicture: 145.0,
                        heightPicture: 82.0,
                        campaign:
                            "Ayo Bantu Para Lansia Terlantara di Wilayah Magelang  ",
                        campaigner: "Yayasan Peduli Lansia",
                      ),
                      ListCampaign(
                        image: "paul",
                        widthPicture: 145.0,
                        heightPicture: 82.0,
                        campaign:
                            "Bantu kakek paul untuk mendapatkan tempat tinggal",
                        campaigner: "Yayasan Peduli Lansia",
                      ),
                    ],
                  ),
                ),
              ],
            ),
          )
        ],
      ),
    );
  }
}
